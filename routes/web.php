<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth::routes();

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::middleware(['auth', 'web'])->group(function () {
	Route::get('/', 'HomeController@index')->name('home');
	Route::resource('admin_user', '\App\Http\Controllers\Admin\AdminUser\AdminController');
	Route::resource('article', '\App\Http\Controllers\Admin\Article\ArticleController');
	Route::resource('category', '\App\Http\Controllers\Admin\Category\CategoryController');
	Route::resource('slider', '\App\Http\Controllers\Admin\Slider\SliderController');
	Route::resource('video', '\App\Http\Controllers\Admin\Video\VideoController');
	Route::resource('big_article', '\App\Http\Controllers\Admin\BigArticle\BigArticleController');
	Route::resource('event', '\App\Http\Controllers\Admin\Event\EventController');

	Route::get('get_tab/{id}/{tab_id}/edit/','\App\Http\Controllers\Admin\Event\Tab\TabController@edit')->name('tab.get');
	Route::get('tab/create/{event_id}','\App\Http\Controllers\Admin\Event\Tab\TabController@create')->name('tab.create');
	Route::post('tab/store/{event_id}', '\App\Http\Controllers\Admin\Event\Tab\TabController@store')->name('tab.store');
  //Route::match(['put', 'patch'], 'update_tab/{id}/{tab_id}','\App\Http\Controllers\Admin\Event\Tab\TabController@update')->name('tab.update');
  	Route::post('update_tab/{id}/{tab_id}','\App\Http\Controllers\Admin\Event\Tab\TabController@update')->name('tab.update');

	Route::match(['delete'], 'delete_tab/{event_id}/{tab_id}', '\App\Http\Controllers\Admin\Event\Tab\TabController@destroy')->name('tab.destroy');
});
