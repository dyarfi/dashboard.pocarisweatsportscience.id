@extends('admin.master.main')
@section('content')
<div class="row">
	<div class="col-md-4">
		<div class="card card-primary">
			<div class="card-header">
				<div class="header-block">
					<p class="title" style="color:white;">Entry Big Article </p>
				</div>
			</div>
			<div class="card-block">
				<br/>
					<h1 class="text-center">{{$big_articles->count()}} <i class="fa fa-newspaper-o"></i></h1>
				<br/>			
			</div>
			<div class="card-footer">
				<a href="{{url('big_article')}}" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-arrow-right"></em> Index</a>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card card-primary">
			<div class="card-header">
				<div class="header-block">
					<p class="title" style="color:white;"> Entry Article </p>
				</div>
			</div>
			<div class="card-block">
				<br/>
				<h1 class="text-center">{{$articles->count()}} <i class="fa fa-newspaper-o"></i></h1>
				<br/>
			</div>
			<div class="card-footer"> <a href="{{url('article')}}" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-arrow-right"></em> Index</a></div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card card-primary">
			<div class="card-header">
				<div class="header-block">
					<p class="title" style="color:white;"> Entry Video </p>
				</div>
			</div>
			<div class="card-block">
				<br/>
					<h1 class="text-center">{{$videos->count()}} <i class="fa fa-video-camera"></i></h1>
				<br/>	
			</div>
			<div class="card-footer"> <a href="{{url('video')}}" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-arrow-right"></em> Index</a></div>
		</div>
	</div>
</div>
<br/>
<div class="row">
	<div class="col-md-4">
		<div class="card card-primary">
			<div class="card-header">
				<div class="header-block">
					<p class="title" style="color:white;">Entry Slider </p>
				</div>
			</div>
			<div class="card-block">
				<br/>
					<h1 class="text-center">{{$sliders->count()}} <i class="fa fa-picture-o"></i></h1>
				<br/>			
			</div>
			<div class="card-footer">
				<a href="{{url('slider')}}" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-arrow-right"></em> Index</a>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card card-primary">
			<div class="card-header">
				<div class="header-block">
					<p class="title" style="color:white;"> Entry Category </p>
				</div>
			</div>
			<div class="card-block">
				<br/>
				<h1 class="text-center">{{$categories->count()}} <i class="fa fa-pencil-square-o"></i></h1>
				<br/>
			</div>
			<div class="card-footer"> <a href="{{url('category')}}" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-arrow-right"></em> Index</a></div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card card-primary">
			<div class="card-header">
				<div class="header-block">
					<p class="title" style="color:white;"> Entry Event </p>
				</div>
			</div>
			<div class="card-block">
				<br/>
					<h1 class="text-center">{{$events->count()}} <i class="fa fa-calendar" aria-hidden="true"></i></h1>
				<br/>	
			</div>
			<div class="card-footer"> <a href="{{url('event')}}" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-arrow-right"></em> Index</a></div>
		</div>
	</div>
</div>

@endsection