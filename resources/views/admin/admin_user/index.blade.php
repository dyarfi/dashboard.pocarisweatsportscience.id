@extends('admin.master.main')

@section('content')
<div class="title-block">
	<h1 class="title"> Admin User </h1>
	<p class="title-description"> Index Table Admin Dashboard </p>
		<a href="{{ route('admin_user.create')}}" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-plus"></em> Add</a>
</div>
@if(session('message'))
	<br/>
  <div class="alert alert-success">
	  {{session('message')}}
  </div>
  <br/>
@endif
<section class="section">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-block">
					<div class="card-title-block">
					</div>
					<section class="example">
						<table class="table table_noxus_ table-hover">
							<thead>
								<tr>
									<th>Name</th>
									<th>Username</th>
									<th>Email</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($users as $user)
								<tr>
									<th>{{ $user->name}}</th>
									<th>{{ $user->username}}</th>
									<th>{{ $user->email }}</th>
									<th>
										<div class="actions_noxus">
											<a class="button_left" href="{{url('admin_user')}}/{{$user->id}}/edit">
												<button type="button" class="btn btn-pill-left btn-info"><em class="fa fa-pencil-square-o"></em></button>
											</a>
					          	<form role="form" class="button_right" action="{{ route ('admin_user.destroy', $user->id) }}" method="POST">
				            		{!! method_field('DELETE') !!}
					              {!! csrf_field() !!}
										  	<button type="button" onclick="return areYouSure(event,this.form)" class="btn btn-pill-right btn-danger"><em class="fa fa-eraser"></em></button>
				            	</form>
										</div>
									</th>
								</tr>
								@endforeach
							</tbody>
						</table>
					</section>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection