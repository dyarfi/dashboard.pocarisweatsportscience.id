@extends('admin.master.main')

@section('content')
<div class="title-block">
  <h1 class="title"> Create Admin</h1>
  <p class="title-description"></p>
</div>
@if(session('message'))
    <br/>
  <div class="alert alert-success">
      {{session('message')}}
  </div>
  <br/>
@endif
@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif
<section class="section">
  <div class="row sameheight-container">
    <div class="container-fluid">
        <div class="card card-block sameheight-item">
            <form action="{{route('admin_user.update', $admin->id)}}" method="POST" enctype="multipart/form-data" role="form">
            {!! method_field('PUT') !!}
            {!! csrf_field() !!}
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Name</label>
                  <input type="text" name="name" placeholder="" class="form-control underlined" value="{{old('name',$admin->name)}}"> 
                </div>
                <div class="form-group">
                  <label class="control-label">Email</label>
                  <input type="email" name="email" placeholder="" class="form-control underlined" value="{{old('email',$admin->email)}}"> 
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Username</label>
                  <input type="text" name="username" placeholder="" class="form-control underlined" value="{{old('username',$admin->username)}}"> 
                </div>
                <div class="form-group">
                  <label class="control-label">Password</label>
                  <input type="password" name="password" placeholder="" class="form-control underlined" value=""> 
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-info pull-right"><em class="fa fa-check-square-o"></em> Submit</button>
            <a href="{{ route('admin_user.index') }}" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-hand-o-left"></em> Cancel</a>
          </form>
        </div>
      </div>
    </div>
  </section>
@endsection