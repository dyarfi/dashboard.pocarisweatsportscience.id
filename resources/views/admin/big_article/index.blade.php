@extends('admin.master.main')

@section('content')
<div class="title-block">
	<h1 class="title"> Big Article | Campaign </h1>
	<p class="title-description"> Index Table Big Article or Campaign </p>
	<a href="{{ route('big_article.create')}}" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-plus"></em> Add</a>
</div>

@if(session('message'))
  <div class="alert alert-success">
	  {{session('message')}}
  </div>
@endif
<section class="section">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-block">
					<div class="card-title-block">
					</div>
					<section class="example">
						<table class="table table_noxus table-hover">
							<thead>
								<tr>
									<th>Title</th>
									<th>Image</th>
									<th>Campaign Name</th>
									<th>Slug</th>
									<th>Date</th>
									<th>Type</th>
									<th>Status</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($big_articles as $article)
								<tr>
									<th>{{$article->title}}</th>
									<th><img class="thumb_noxs" src="{{asset('uploads')}}/{{@$article->campaign['image']}}"/></th>
									<th>{{$article->campaign['name']}}</th>
									<th>{{$article->slug}}</th>
									<th>
										@if($article->date)
										<?php
											$utcdatetime = $article->date;
										  $datetime = $utcdatetime->toDateTime();
										  $time=$datetime->format('m/d/Y H:m:s');
										?>
										{{$time }}
										@endif
									</th>
									<th>{{$article->type}}</th>
									<th>{{$article->status ? "Published" : "Unpublished" }}</th>
									<th width="150px;">
										<div class="actions_noxus">
											<a class="button_left" href="{{url('big_article')}}/{{$article->id}}/edit">
												<button type="button" class="btn btn-pill-left btn-info"><em class="fa fa-pencil-square-o"></em></button>
											</a>
					          	<form class="button_right" role="form" action="{{ route ('big_article.destroy', $article->id) }}" method="POST">
				            		{!! method_field('DELETE') !!}
					              {!! csrf_field() !!}
										  	<button onclick="return areYouSure(event,this.form)" class="btn btn-pill-right btn-danger" type="submit"><em class="fa fa-eraser"></em></button>
				            	</form>
				            </div>
									</th>
								</tr>
									@endforeach
							</tbody>
						</table>
					</section>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
