@extends('admin.master.main')

@section('content')
<div class="title-block">
  <h1 class="title"> Create Big Article</h1>
  <p class="title-description"></p>
</div>
@if(session('message'))
    <br/>
  <div class="alert alert-success">
      {{session('message')}}
  </div>
  <br/>
@endif
@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif
<section class="section">
  <div class="row sameheight-container">
    <div class="container-fluid">
        <div class="card card-block">
          <form action="{{route('big_article.update', $big_article->id)}}" method="POST" enctype="multipart/form-data" role="form">
            {!! method_field('PUT') !!}
            {!! csrf_field() !!}
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Title</label>
                  <input type="text" name="title" placeholder="" class="form-control underlined" value="{{ old('title',$big_article->title)}}">
                </div>
                <div class="form-group">
                  <label class="control-label">Campaign Name</label>
                  <input type="text" name="campaign[name]" placeholder="" class="form-control underlined" value="{{old('campaign[name]', $big_article->campaign['name'])}}">
                </div>
                <div class="form-group">
                  <label class="control-label">Date</label>
                  <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                    <input  name="date" value="{{ old('date',$big_article->date)}}" class="form-control datetimepicker-input underlined" data-target="#datetimepicker1"  />
                    <span class="input-group-addon" data-target="#datetimepicker1" data-toggle="datetimepicker">
                    <span class="fa fa-calendar"></span>
                    </span>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Caption</label>
                  <input type="text" name="campaign[caption]" placeholder="" class="form-control underlined" value="{{ old('campaign[caption]',$big_article->campaign['caption'])}}">
                </div>
                <div class="form-group">
                  <label class="control-label">Main Slug</label>
                  <input type="text" name="campaign[slug]" placeholder="" class="form-control underlined" value="{{ old('campaign[slug]',$big_article->campaign['slug'])}}">
                </div>
                <div class="form-group">
                  <label class="control-label">Sub Slug</label>
                  <input type="text" name="slug" placeholder="" class="form-control underlined" value="{{ old('slug',$big_article->slug)}}">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label">Content</label>
                  <div name="content" class="editor_noxus">{!! old('content',$big_article->content) !!}</div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="thumb_image cont">
                  <img src="{{asset('uploads')}}/{{@$big_article->campaign['image']}}" alt="" class="thumb_image"/>
                  <span>{{@$big_article->campaign['image']}}</span>
                </div>
                <div class="form-group">
                  <label class="control-label">Image(1200x530)</label>
                  <input type="file" name="image" placeholder="" class="form-control underlined" value="">
                </div>
                <div class="thumb_image cont">
                  <img src="{{asset('uploads')}}/{{@$big_article->campaign['image_mobile']}}" alt="" class="thumb_image"/>
                  <span>{{@$big_article->campaign['image_mobile']}}</span>
                </div>
                <div class="form-group">
                  <label class="control-label">Image Mobile(500x650)</label>
                  <input type="file" name="image_mobile" placeholder="" class="form-control underlined" value="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Header Text</label>
                  <select name="header_type" class="selector_noxus">
                    <option value="{{old('header_type',$big_article->header_type)}}" selected>{{$big_article->header_type}}</option>
                    <option value="text-right">text-right</option>
                    <option value="text-left">text-left</option>
                    <option value="text-center">text-center</option>
                    <option value="without-text">without-text</option>
                  </select>
                </div>
                <div class="form-group">
                  <label class="control-label">Status</label>
                  <select name="status" class="selector_noxus">
                    <option value="{{old('status',$big_article->status)}}" selected>{{$big_article->status ? "Published" : "Unpublished"}}</option>
                    <option value="1">Published</option>
                    <option value="0">Unpublished</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group pull-right">
                  <button type="submit" class="btn btn-info pull-right"><em class="fa fa-check-square-o"></em> Submit</button>
                  <a href="{{ route('big_article.index') }}" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-hand-o-left"></em> Cancel</a>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
@endsection
