@extends('admin.master.main')

@section('content')
<div class="title-block">
	<h1 class="title"> Category </h1>
	<p class="title-description"> Index Table Category </p>
	<a href="{{ route('category.create')}}" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-plus"></em> Add</a>
</div>

@if(session('message'))
  <div class="alert alert-success">
	  {{session('message')}}
  </div>
@endif
<section class="section">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-block">
					<div class="card-title-block">
					</div>
					<section class="example">
						<table class="table table_noxus table-hover">
							<thead>
								<tr>
									<th>Name</th>
									<th>Alias</th>
									<th>Status</th>
									<th>Type</th>
									<th>Order Number</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($categories as $category)
								<tr>
									<th>{{$category->name}}</th>
									<th>{{$category->alias}}</th>
									<th>{{$category->status ? "True" : "False" }}</th>
									<th>{{$category->type}}</th>
									<th class="sort">{{$category->order_number}}</th>
									<th>
										<div class="actions_noxus">
											<a class="button_left" href="{{url('category')}}/{{$category->id}}/edit">
												<button type="button" class="btn btn-pill-left btn-info"><em class="fa fa-pencil-square-o"></em></button>
											</a>
					          	<form class="button_right" role="form" action="{{ route ('category.destroy', $category->id) }}" method="POST">
				            		{!! method_field('DELETE') !!}
					              {!! csrf_field() !!}
										  	<button onclick="return areYouSure(event,this.form)" class="btn btn-pill-right btn-danger" type="submit"><em class="fa fa-eraser"></em></button>
				            	</form>
				            </div>
									</th>
								</tr>
									@endforeach
							</tbody>
						</table>
					</section>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection