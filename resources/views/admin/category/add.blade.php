@extends('admin.master.main')

@section('content')
<div class="title-block">
  <h1 class="title"> Create Category</h1>
  <p class="title-description"></p>
</div>
@if(session('message'))
    <br/>
  <div class="alert alert-success">
      {{session('message')}}
  </div>
  <br/>
@endif
@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif
<section class="section">
  <div class="row sameheight-container">
    <div class="container-fluid">
        <div class="card card-block sameheight-item">
            <form action="{{action ('Admin\Category\CategoryController@store')}}" method="POST" role="form">
            {!! csrf_field() !!}
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Name</label>
                  <input type="text" name="name" placeholder="" class="form-control underlined" value=""> 
                </div>
                <div class="form-group">
                  <label class="control-label">Alias</label>
                  <input type="text" name="alias" placeholder="" class="form-control underlined" value=""> 
                </div>
                <div class="form-group">
                  <label class="control-label">Type</label>
                  <input type="text" name="type" placeholder="" class="form-control underlined" value=""> 
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Order Number</label>
                  <input type="text" name="order_number" placeholder="" class="form-control underlined" value=""> 
                </div>
                <div class="form-group">
                  <label class="control-label">Status</label>
                  <select name="status" class="selector_noxus">
                    <option value="" selected>Select Your Status</option>
                    <option value="1">True</option>
                    <option value="0">False</option>
                  </select>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-info pull-right"><em class="fa fa-check-square-o"></em> Submit</button>
            <a href="{{ route('category.index') }}" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-hand-o-left"></em> Cancel</a>
          </form>
        </div>
      </div>
    </div>
  </section>
@endsection