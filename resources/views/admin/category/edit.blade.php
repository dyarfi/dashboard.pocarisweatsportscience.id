@extends('admin.master.main')

@section('content')
<div class="title-block">
    <h1 class="title"> Edit Category</h1>
    <p class="title-description">  Name : {{$category->name }} </p>
</div>
@if(session('message'))
    <br/>
  <div class="alert alert-success">
      {{session('message')}}
  </div>
  <br/>
@endif
<section class="section">
  <div class="row sameheight-container">
    <div class="container-fluid">
        <div class="card card-block sameheight-item">
          <form action="{{route ('category.update', $category->id) }}" method="POST" enctype="multipart/form-data" role="form">
            {!! method_field('PUT') !!}
            {!! csrf_field() !!}
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Name</label>
                  <input type="text" name="name" placeholder="{{ old ('name', $category->name) }}" class="form-control underlined" value="{{ old ('name', $category->name) }}">
                </div>
                <div class="form-group">
                  <label class="control-label">Alias</label>
                  <input type="text" name="alias" placeholder="{{ old ('alias', $category->alias) }}" class="form-control underlined" value="{{ old ('alias', $category->alias) }}">
                </div>
                <div class="form-group">
                  <label class="control-label">Type</label>
                  <input type="text" name="type" placeholder="{{ old ('type', $category->type) }}" class="form-control underlined" value="{{ old ('type', $category->type) }}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Order Number</label>
                  <input type="text" name="order_number" placeholder="{{ old ('order_number', $category->order_number) }}" class="form-control underlined" value="{{ old ('order_number', $category->order_number) }}">
                </div>
                <div class="form-group">
                  <label class="control-label">Status</label>
                  <select name="status" class="selector_noxus">
                    <option value="{{ old ('status', $category->status) }}" selected>{{old ('status', $category->status) == '1' ? 'True' : 'False'}}</option>
                    <option value="1">True</option>
                    <option value="0">False</option>
                  </select>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-info pull-right"><em class="fa fa-check-square-o"></em> Submit</button>
            <a href="{{ route('category.index') }}" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-hand-o-left"></em> Cancel</a>
          </form>
        </div>
      </div>
    </div>
  </section>
@endsection
