@extends('admin.master.main')

@section('content')
<div class="title-block">
  <h1 class="title">Create Slider</h1>
  <p class="title-description"></p>
</div>
@if(session('message'))
    <br/>
  <div class="alert alert-success">
      {{session('message')}}
  </div>
  <br/>
@endif
@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif
<section class="section">
  <div class="row sameheight-container">
    <div class="container-fluid">
        <div class="card card-block sameheight-item">
            <form action="{{action ('Admin\Slider\SliderController@store')}}" method="POST" enctype="multipart/form-data" role="form">
            {!! csrf_field() !!}
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Image</label>
                  <input type="file" name="image" placeholder="" class="form-control underlined" value="">
                </div>
                <div class="form-group">
                  <label class="control-label">Url</label>
                  <input type="text" name="url" placeholder="" class="form-control underlined" value=""> 
                </div>
                <div class="form-group">
                  <label class="control-label">Order Number</label>
                  <input type="text" name="order_number" placeholder="" class="form-control underlined" value=""> 
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Image Mobile</label>
                  <input type="file" name="image_mobile" placeholder="" class="form-control underlined" value="">
                </div>
                <div class="form-group">
                  <label class="control-label">Status</label>
                  <select name="status" class="selector_noxus">
                    <option value="" selected>Select Your Status</option>
                    <option value="1">True</option>
                    <option value="0">False</option>
                  </select>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-info pull-right"><em class="fa fa-check-square-o"></em> Submit</button>
            <a href="{{ route('slider.index') }}" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-hand-o-left"></em> Cancel</a>
          </form>
        </div>
      </div>
    </div>
  </section>
@endsection