@extends('admin.master.main')

@section('content')
<div class="title-block">
	<h1 class="title"> Slider </h1>
	<p class="title-description"> Index Table Slider </p>
	<a href="{{ route('slider.create')}}" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-plus"></em> Add</a>
</div>

@if(session('message'))
  <div class="alert alert-success">
	  {{session('message')}}
  </div>
@endif
<section class="section">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-block">
					<div class="card-title-block">
					</div>
					<section class="example">
						<table class="table table_noxus table-hover">
							<thead>
								<tr>
									<th>Image</th>
									<th>Image Mobile</th>
									<th>Url</th>
									<th>Order Number</th>
									<th>Status</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($sliders as $slider)
								<tr>
									<th><img class="thumb_noxs" src="{{asset('uploads')}}/{{$slider->image}}"/></th>
									<th><img class="thumb_noxs" src="{{asset('uploads')}}/{{$slider->image_mobile}}"/></th>
									<th style="max-width:200px;">{{ $slider->url }}</th>
									<th>{{ $slider->order_number }}</th>
									<th>{{ $slider->status ? "True" : "False" }}</th>
									<th style="width:150px;">
										<div class="actions_noxus">
											<a class="button_left" href="{{url('slider')}}/{{$slider->id}}/edit">
												<button type="button" class="btn btn-pill-left btn-info"><em class="fa fa-pencil-square-o"></em></button>
											</a>
					          	<form class="button_right" role="form" action="{{ route ('slider.destroy', $slider->id) }}" method="POST">
				            		{!! method_field('DELETE') !!}
					              {!! csrf_field() !!}
										  	<button onclick="return areYouSure(event,this.form)" class="btn btn-pill-right btn-danger" type="submit"><em class="fa fa-eraser"></em></button>
				            	</form>
				            </div>
									</th>
								</tr>
								@endforeach
							</tbody>
						</table>
					</section>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection