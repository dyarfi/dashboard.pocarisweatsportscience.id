@extends('admin.master.main')

@section('content')
<div class="title-block">
  <h1 class="title"> Edit Video</h1>
  <p class="title-description">Title : "{!!$video->title!!}"</p>
</div>
@if(session('message'))
    <br/>
  <div class="alert alert-success">
      {{session('message')}}
  </div>
  <br/>
@endif
@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif
<div class="video-container">
  <iframe width="560" height="349" src="{{ old('caption',$video->caption)}}" frameborder="0" allowfullscreen></iframe>
</div>
<section class="section">
  <div class="row sameheight-container">
    <div class="container-fluid">
        <div class="card card-block">
          <form action="{{route('video.update', $video->id)}}" method="POST" enctype="multipart/form-data" role="form">
            {!! method_field('PUT') !!}
            {!! csrf_field() !!}
            <div class="row">

            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Title</label>
                  <input type="text" name="title" placeholder="" class="form-control underlined" value="{{ old('title',$video->title)}}"> 
                </div>
                <div class="form-group">
                  <label class="control-label">Category</label>
                  <select name="category" class="selector_noxus">
                    <option value="{{$video->category}}" selected>{{$video->category}}</option>
                    @foreach($categories as $category)
                    <option value="{{$category->name}}">{{$category->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label class="control-label">Date</label>
                  <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                    <input  name="date" value="{{ old('date',$video->date)}}" class="form-control datetimepicker-input underlined" data-target="#datetimepicker1"  />
                    <span class="input-group-addon" data-target="#datetimepicker1" data-toggle="datetimepicker">
                    <span class="fa fa-calendar"></span>
                    </span>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Embed Video</label>
                  <input type="text" name="caption" placeholder="" class="form-control underlined" value="{{ old('caption',$video->caption)}}"> 
                </div>
                <div class="form-group">
                  <label class="control-label">Tag</label>
                  <input type="text" name="tag" placeholder="" class="form-control underlined" value="{{ old('tag',$video->tag)}}"> 
                </div>
                <div class="form-group">
                  <label class="control-label">Writer</label>
                  <input type="text" name="writer[name]" placeholder="" class="form-control underlined" value="{{$video->writer['name']}}"> 
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label">Content</label>
                  <div name="content" class="editor_noxus">{!! old('content',$video->content) !!}</div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Columns</label>
                  <select name="columns" class="selector_noxus">
                    <option value="{{old('columns',$video->columns)}}" selected>{{$video->columns}}</option>
                    <option value="big">Big</option>
                    <option value="small">Small</option>
                  </select>
                </div>
                <div class="thumb_image cont">
                  <img src="{{asset('uploads')}}/{{$video->image}}" alt="" class="thumb_image"/>
                  <span>{{$video->image}}</span>
                </div>
                <div class="form-group">
                  <label class="control-label">Image(1200x530)</label>
                  <input type="file" name="image" placeholder="" class="form-control underlined" value="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Status</label>
                  <select name="status" class="selector_noxus">
                    <option value="{{old('status',$video->status)}}" selected>{{$video->status ? "Published" : "Unpublished"}}</option>
                    <option value="1">Published</option>
                    <option value="0">Unpublished</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group pull-right">
                  <button type="submit" class="btn btn-info pull-right"><em class="fa fa-check-square-o"></em> Submit</button>
                  <a href="{{ route('category.index') }}" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-hand-o-left"></em> Cancel</a>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
@endsection