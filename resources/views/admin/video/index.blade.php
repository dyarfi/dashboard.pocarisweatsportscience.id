@extends('admin.master.main')

@section('content')
<div class="title-block">
	<h1 class="title"> Video </h1>
	<p class="title-description"> Index Table Video </p>
	<a href="{{ route('video.create')}}" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-plus"></em> Add</a>
</div>

@if(session('message'))
  <div class="alert alert-success">
	  {{session('message')}}
  </div>
@endif
<section class="section">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-block">
					<div class="card-title-block">
					</div>
					<section class="example">
						<table class="table table_noxus table-hover">
							<thead>
								<tr>
									<th>Image</th>
									<th>Title</th>
									<th>Type</th>
									<th>Category</th>
									<th>Date</th>
									<th>Tag</th>
									<th>Status</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($videos as $video)
								<tr>
									<th><img class="thumb_noxs" src="{{asset('uploads')}}/{{$video->image}}"/></th>
									<th>{!!$video->title!!}</th>
									<th>{{$video->type}}</th>
									<th>{{$video->category}}</th>
									<th>
										@if($video->date)
										<?php 
											$utcdatetime = $video->date;
										  $datetime = $utcdatetime->toDateTime();
										  $time=$datetime->format('m/d/Y H:m:s');
										?>
										{{$time }}
										@endif
									</th>
									<th>{{$video->tag}}</th>
									<th>{{$video->status ? "Published" : "Unpublished" }}</th>
									<th width="150px;">
										<div class="actions_noxus">
											<a class="button_left" href="{{url('video')}}/{{$video->id}}/edit">
												<button type="button" class="btn btn-pill-left btn-info"><em class="fa fa-pencil-square-o"></em></button>
											</a>
					          	<form class="button_right" role="form" action="{{ route ('video.destroy', $video->id) }}" method="POST">
				            		{!! method_field('DELETE') !!}
					              {!! csrf_field() !!}
										  	<button onclick="return areYouSure(event,this.form)" class="btn btn-pill-right btn-danger" type="submit"><em class="fa fa-eraser"></em></button>
				            	</form>
				            </div>
									</th>
								</tr>
									@endforeach
							</tbody>
						</table>
					</section>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection