<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title> Pocaris Sweat Sport Science | CMS </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Place favicon.ico in the root directory -->
  <link rel="shortcut icon" href="{{asset('ico/favicon.ico')}}" />
  <link rel="icon" type="image/png" href="{{asset('ico/pocarisweat_ico.png')}}" />
  <link rel="stylesheet" href="{{asset('admin/css/noxus_compiled.css')}} ">
</head>
<body>