@include('admin.master.header')
	<div class="app" id="app">
		@include('admin.master.menu')
		@include('admin.master.sidebar')
		<article class="content dashboard-page">
			@yield('content')
		</article>
		@include('admin.master.menu_footer')
	</div>
@include('admin.master.footer')
