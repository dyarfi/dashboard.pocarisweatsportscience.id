      <aside class="sidebar">
        <div class="sidebar-container">
          <div class="sidebar-header">
            <div class="brand">
              <div class="logo">
                <img src="{{asset('logo.png')}}"/>
              </div>
            </div>
          </div>
          <nav class="menu">
            <ul class="sidebar-menu metismenu" id="sidebar-menu">
              <li {{ setActive('/') }}>
                <a href="{{url('/')}}">
                <i class="fa fa-home"></i> Dashboard </a>
              </li>
              <li {{ setActive('admin_user')}}>
                <a href="{{url('admin_user')}}">
                <i class="fa fa-user"></i> Admin </a>
              </li>
              <li {{ setActive('category') }}>
                <a href="{{url('category')}}">
                <i class="fa fa-pencil-square-o"></i> Category </a>
              </li>
              <li {{ setActive('slider') }}>
                <a href="{{url('slider')}}">
                <i class="fa fa-picture-o"></i> Slider </a>
              </li>
              <li {{ setActive('article') }}>
                <a href="{{url('article')}}">
                <i class="fa fa-newspaper-o"></i> Article </a>
              </li>
              <li {{ setActive('big_article') }}>
                <a href="{{url('big_article')}}">
                <i class="fa fa-newspaper-o"></i> Big Article </a>
              </li>
              <li {{ setActive('event') }}>
                <a href="{{url('event')}}">
                <i class="fa fa-calendar" aria-hidden="true"></i> Event </a>
              </li>
              <li {{ setActive('video') }}>
                <a href="{{url('video')}}">
                <i class="fa fa-video-camera"></i> Video </a>
              </li>
<!--               <li class="open">
                <a href="">
                    <i class="fa fa-sitemap"></i> Menu Levels
                    <i class="fa arrow"></i>
                </a>
                <ul class="sidebar-nav collapse in" style="">
                  <li>
                    <a href="#"> Second Level Item </a>
                  </li>
                </ul>
              </li> -->
            </ul>
          </nav>
        </div>
      </aside>
      <div class="sidebar-overlay" id="sidebar-overlay"></div>
      <div class="sidebar-mobile-menu-handle" id="sidebar-mobile-menu-handle"></div>
      <div class="mobile-menu-handle"></div>