    <form action="{{route('event.update', $event->id)}}" method="POST" enctype="multipart/form-data" role="form">
      {!! method_field('PUT') !!}
      {!! csrf_field() !!}
    <section class="section">
      <div class="row sameheight-container">
        <div class="container-fluid">
          <div class="card card-block">
            <div class="row">
              <div class="col-md-12">
                <div class="title-block">
                  <h1 class="title"> <b>Event Detail</b></h1>
                  <p class="title-description"><hr/></p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Title</label>
                  <input type="text" name="title" placeholder="" class="form-control underlined" value="{{ old('title',$event->title)}}">
                </div>
                <div class="form-group">
                  <label class="control-label">Columns</label>
                  <select name="columns" class="selector_noxus">
                    <option value="{{old('columns',$event->columns)}}" selected>{{$event->columns}}</option>
                    <option value="big">Big</option>
                    <option value="small">Small</option>
                  </select>
                </div>
                <div class="form-group">
                  <label class="control-label">Tag</label>
                  <input type="text" name="tag" placeholder="" class="form-control underlined" value="{{ old('tag',$event->tag)}}">
                </div>
                <div class="form-group">
                  <label class="control-label" for="video">Video</label>
                  <input type="text" name="video" id="video" placeholder="Video Embed https://www.youtube.com/embed/xxxxx" class="form-control underlined" value="{{old('video',$event->video)}}">
                </div>
                <div class="form-group">
                  <label class="control-label" for="register">Register Link</label>
                  <input type="text" name="register" id="register" placeholder="http://" class="form-control underlined" value="{{old('register',$event->register)}}">
                </div>
                <div class="form-group">
                  <label class="control-label">Status</label>
                  <select name="status" class="selector_noxus">
                    <option value="{{old('status',$event->status)}}" selected>{{$event->status ? "Published" : "Unpublished"}}</option>
                    <option value="1">Published</option>
                    <option value="0">Unpublished</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Header Text</label>
                  <select name="header_type" class="selector_noxus">
                    <option value="{{old('header_type',$event->header_type)}}" selected>{{$event->header_type}}</option>
                    <option value="text-right">text-right</option>
                    <option value="text-left">text-left</option>
                    <option value="text-center">text-center</option>
                    <option value="without-text">without-text</option>
                  </select>
                </div>
                @if(isset($information['countdown']))
                <fieldset>
                  <legend>Countdown</legend>
                  <div class="form-group">
                    <label class="control-label" for="content[countdown][title]">Title</label>
                    <input type="text" name="content[countdown][title]" id="content[countdown][title]" placeholder="" class="form-control underlined" value="{{old('content[countdown][title]',$information['countdown']['title'])}}">
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="content[countdown][btn_title]">Button Title</label>
                    <input type="text" name="content[countdown][btn_title]" id="content[countdown][btn_title]" placeholder="" class="form-control underlined" value="{{old('content[countdown][btn_title]',$information['countdown']['btn_title'])}}">
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="content[countdown][btn_class]">Button Class</label>
                    <input type="text" name="content[countdown][btn_class]" id="content[countdown][btn_class]" placeholder="" class="form-control underlined" value="{{old('content[countdown][btn_class]',$information['countdown']['btn_class'])}}">
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="content[countdown][btn_url]">Button Url</label>
                    <input type="text" name="content[countdown][btn_url]" id="content[countdown][btn_url]" placeholder="" class="form-control underlined" value="{{old('content[countdown][btn_url]',$information['countdown']['btn_url'])}}">
                  </div>
                  <div class="col-md-12">
                    <div class="row">
                      <div class="form-group">
                        <label class="control-label">Countdown Date</label>
                        <?php
                        if (@$information['countdown']['countdown_date']) {
                          $datetime = old('content[countdown[countdown_date]]',$information['countdown']['countdown_date']->toDateTime());
                          $date = $datetime->format('m/d/Y g:i A');
                        } else {
                          $date = '';
                        }
                        ?>
                        <div class="input-group content[countdown[countdown_date]]" id="datetimepicker4" data-target-input="nearest">
                          <input name="content[countdown][countdown_date]" value="{!! $date !!}" class="form-control datetimepicker-input underlined" data-target="#datetimepicker4"  />
                          <span class="input-group-addon" data-target="#datetimepicker4" data-toggle="datetimepicker">
                          <span class="fa fa-calendar"></span>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </fieldset>
                @else
                <fieldset>
                  <legend>Countdown</legend>
                  <div class="form-group">
                    <label class="control-label" for="content[countdown][title]">Title</label>
                    <input type="text" name="content[countdown][title]" id="content[countdown][title]" placeholder="" class="form-control underlined" value="">
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="content[countdown][btn_title]">Button Title</label>
                    <input type="text" name="content[countdown][btn_title]" id="content[countdown][btn_title]" placeholder="" class="form-control underlined" value="">
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="content[countdown[btn_class]]">Button Class</label>
                    <input type="text" name="content[countdown[btn_class]]" id="content[countdown[btn_class]]" placeholder="" class="form-control underlined" value="">
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="content[countdown[btn_url]]">Button Url</label>
                    <input type="text" name="content[countdown[btn_url]]" id="content[countdown[btn_url]]" placeholder="" class="form-control underlined" value="">
                  </div>
                  <div class="col-md-12">
                    <div class="row">
                      <div class="form-group">
                        <label class="control-label">Countdown Date</label>
                        <div class="input-group content[countdown[countdown_date]]" id="datetimepicker4" data-target-input="nearest">
                          <input name="content[countdown][countdown_date]" value="" class="form-control datetimepicker-input underlined" data-target="#datetimepicker4"  />
                          <span class="input-group-addon" data-target="#datetimepicker4" data-toggle="datetimepicker">
                          <span class="fa fa-calendar"></span>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </fieldset>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
      <br/>
      <div class="row sameheight-container">
        <div class="container-fluid">
          <div class="card card-block">
            <div class="row">
              <div class="col-md-12">
                <div class="title-block">
                  <h1 class="title"> <b>Images</b></h1>
                  <p class="title-description"><hr/></p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="thumb_image cont">
                  <img src="{{asset('uploads')}}/{{$event->image}}" alt="" class="thumb_image"/>
                  <span>{{$event->image}}</span>
                </div>
                <div class="form-group">
                  <label class="control-label" for="image">Image(1200x530)</label>
                  <input type="file" name="image" id="image" placeholder="" class="form-control underlined" value="">
                </div>
                <div class="thumb_image cont">
                  <img style="width:100%;" src="{{asset('uploads')}}/{{$event->image_mobile}}" alt="" class="thumb_image"/>
                  <span>{{$event->image_mobile}}</span>
                </div>
                <div class="form-group">
                  <label class="control-label" for="image_mobile">Image Mobile(500x650)</label>
                  <input type="file" name="image_mobile" id="image_mobile" placeholder="" class="form-control underlined" value="">
                </div>
                <div class="thumb_image cont">
                  <img src="{{asset('uploads')}}/{{$event->image_thumbnail}}" alt="" class="thumb_image"/>
                  <span>{{$event->image_thumbnail}}</span>
                </div>
                <div class="form-group">
                  <label class="control-label">Image Thumbnail(Potrait)</label>
                  <input type="file" name="image_thumbnail" placeholder="" class="form-control underlined" value="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="thumb_image cont">
                  <img style="width:100%;" src="{{asset('uploads')}}/{{$event->image_background}}" alt="" class="thumb_image"/>
                  <span>{{$event->image_background}}</span>
                </div>
                <div class="form-group">
                  <label class="control-label">Image Background(1240x960)</label>
                  <input type="file" name="image_background" placeholder="" class="form-control underlined" value="">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br/>
      <div class="row sameheight-container">
        <div class="container-fluid">
          <div class="card card-block">
            <div class="row">
              <div class="col-md-12">
                <div class="title-block">
                  <h1 class="title"> <b>Event Information</b></h1>
                  <p class="title-description"><hr/></p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Event Date</label>
                  <?php
                  if (@$information['event_date']) {
                    $datetime = old('content[event_date]',$information['event_date']->toDateTime());
                    $date = $datetime->format('m/d/Y g:i A');
                  } else {
                    $date = '';
                  }
                  ?>
                  <div class="input-group content[event_date]" id="datetimepicker2" data-target-input="nearest">
                    <input name="content[event_date]" value="{!! $date !!}" class="form-control datetimepicker-input underlined" data-target="#datetimepicker2"  />
                    <span class="input-group-addon" data-target="#datetimepicker2" data-toggle="datetimepicker">
                    <span class="fa fa-calendar"></span>
                    </span>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label">Content</label>
                  <div name="content[body_content]" class="editor_noxus">
                    {!! old('content[body_content]',$information['body_content']) !!}
                  </div>
                </div>
              </div>
            </div>
            <hr/>
          </div>
        </div>
      </div>
      <br/>
      <div class="row sameheight-container">
        <div class="container-fluid">
          <div class="card card-block">
            <div class="row">
              <div class="col-md-12">
                <div class="title-block">
                  <h1 class="title"> <b>CTA</b></h1>
                  <p class="title-description"><hr/></p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                {{--  content[0].countdown.title  --}}
                {{--  content[0].countdown.btn_url --}}
                {{--  content[0].countdown.btn_class  --}}
                {{--  content[0].countdown.btn_target --}}
                <div class="form-group">
                  <label class="control-label">Button Title</label>
                  <input type="text" name="content[body_button][0][btn_title]" placeholder='<i class="fa fa-phone"></i> Abay <br> 0813 8036 7630' class="form-control underlined" value="{{old('content[body_button][0][btn_title]', $information['body_button'][0]['btn_title'])}}">
                </div>
                <div class="form-group">
                  <label class="control-label">Button Link</label>
                  <input type="text" name="content[body_button][0][btn_url]" placeholder="tel:0211234567" class="form-control underlined" value="{{old('content[body_button][0][btn_url]', $information['body_button'][0]['btn_url'])}}">
                </div>
                <div class="form-group">
                  <label class="control-label">Columns</label>
                  <input type="text" name="content[body_button][0][columns]" placeholder="col-md-4" class="form-control underlined" value="{{old('content[body_button][0][columns]', $information['body_button'][0]['columns'])}}">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label">Button Title</label>
                  <input type="text" name="content[body_button][1][btn_title]" placeholder='<i class="fa fa-phone"></i> Abay <br> 0813 8036 7630' class="form-control underlined" value="{{old('content[body_button][1][btn_title]', $information['body_button'][1]['btn_title'])}}">
                </div>
                <div class="form-group">
                  <label class="control-label">Button Link</label>
                  <input type="text" name="content[body_button][1][btn_url]" placeholder="tel:0211234567" class="form-control underlined" value="{{old('content[body_button][1][btn_url]', $information['body_button'][1]['btn_url'])}}">
                </div>
                <div class="form-group">
                  <label class="control-label">Columns</label>
                  <input type="text" name="content[body_button][1][columns]" placeholder="col-md-4" class="form-control underlined" value="{{old('content[body_button][1][columns]', $information['body_button'][1]['columns'])}}">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label">Button Title</label>
                  <input type="text" name="content[body_button][2][btn_title]" placeholder='<i class="fa fa-phone"></i> Abay <br> 0813 8036 7630' class="form-control underlined" value="{{old('content[body_button][2][btn_title]', $information['body_button'][2]['btn_title'])}}">
                </div>
                <div class="form-group">
                  <label class="control-label">Button Link</label>
                  <input type="text" name="content[body_button][2][btn_url]" placeholder="tel:0211234567" class="form-control underlined" value="{{old('content[body_button][2][btn_url]', $information['body_button'][2]['btn_url'])}}">
                </div>
                <div class="form-group">
                  <label class="control-label">Columns</label>
                  <input type="text" name="content[body_button][2][columns]" placeholder="col-md-4" class="form-control underlined" value="{{old('content[body_button][2][columns]', $information['body_button'][2]['columns'])}}">
                </div>
              </div>
            </div>
            <hr/>
          </div>
        </div>
      </div>
      <br/>
      <div class="row sameheight-container">
        <div class="container-fluid">
          <div class="card card-block">
            <div class="row">
              <div class="col-md-12">
                <div class="title-block">
                  <h1 class="title"><b>Related Tabs</b></h1>
                  <p class="title-description"><hr/></p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Content Tag Article</label>
                  <input type="text" name="content_tag_article" placeholder="Insert Tag Articles" class="form-control underlined" value="{{old('content[1][content_tag]', $article['content_tag'])}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Content Tag Gallery</label>
                  <input type="text" name="content_tag_gallery" placeholder="Insert Tag Gallery" class="form-control underlined" value="{{old('content[2][content_tag]', $gallery['content_tag'])}}">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br/>
      <div class="row sameheight-container">
        <div class="container-fluid">
          <div class="card card-block">
            <div class="row">
              <div class="col-md-12">
                <div class="title-block">
                  <h1 class="title"><b>Left Sidebar</b></h1>
                  <p class="title-description"><hr/></p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label">Event Type</label>
                  <input type="hidden" name="content[left_sidebar][list][0][field]" placeholder="" class="form-control underlined" value="Event Type">
                  <input type="text" name="content[left_sidebar][list][0][value]" placeholder="Futsal" class="form-control underlined" value="{{old('content[left_sidebar][0][list][0][value]', $information['left_sidebar']['0']['list']['0']['value'])}}">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label">Venue</label>
                  <input type="hidden" name="content[left_sidebar][list][1][field]" placeholder="" class="form-control underlined" value="Venue">
                  <input type="text" name="content[left_sidebar][list][1][value]" placeholder="GOR C-TRA ARENA" class="form-control underlined" value="{{old('content[left_sidebar][0][list][1][value]', $information['left_sidebar']['0']['list']['1']['value'])}}">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label">Date</label>
                  <input type="hidden" name="content[left_sidebar][list][2][field]" placeholder="" class="form-control underlined" value="Date">
                  <input type="text" name="content[left_sidebar][list][2][value]" placeholder="example : 12-15 January 2020" class="form-control underlined" value="{{old('content[left_sidebar][0][list][2][value]', $information['left_sidebar']['0']['list']['2']['value'])}}">
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label">RPC Date</label>
                  <input type="hidden" name="content[left_sidebar][list][3][field]" placeholder="" class="form-control underlined" value="RPC Date">
                  <input type="hidden" name="content[left_sidebar][list][3][value]" value="{{old('content[left_sidebar][0][list][3][value]', @$information['left_sidebar']['0']['list']['3']['value'])}}">
                  <div name="content[left_sidebar][list][3][value]" class="editor_noxus">
                      {!!old('content[left_sidebar][0][list][3][value]', @$information['left_sidebar']['0']['list']['3']['value'])!!}
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label">RPC Location</label>
                  <input type="hidden" name="content[left_sidebar][list][4][field]" placeholder="" class="form-control underlined" value="RPC Location">
                  <input type="hidden" name="content[left_sidebar][list][4][value]" value="{{old('content[left_sidebar][0][list][4][value]', @$information['left_sidebar']['0']['list']['4']['value'])}}">
                  <div name="content[left_sidebar][list][4][value]" class="editor_noxus">
                      {!!old('content[left_sidebar][0][list][4][value]', @$information['left_sidebar']['0']['list']['4']['value'])!!}
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group pull-right">
                  <button type="submit" class="btn btn-info pull-right"><em class="fa fa-check-square-o"></em> Submit</button>
                  <a href="{{ route('event.index') }}" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-hand-o-left"></em> Cancel</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      </section>
      <input type="hidden" name="content[left_sidebar][title]" placeholder="" class="form-control underlined" value="EVENT INFORMATION">
      <input type="hidden" name="content[left_sidebar][type]" placeholder="" class="form-control underlined" value="vertical">
      <input type="hidden" name="content[status]" placeholder="" class="form-control underlined" value="1">
      <input type="hidden" name="content[type]" placeholder="" class="form-control underlined" value="a">
    </form>
