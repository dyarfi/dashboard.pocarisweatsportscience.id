  <div class="tab-pane fade" id="tab-index" aria-expanded="false">
    <br/>
    <section class="section">
      <div class="row sameheight-container">
        <div class="container-fluid">
          <div class="card card-block">
            <div class="row">
              <div class="col-md-12">
                <div class="title-block">
                  <h1 class="title"> <b>Event Tabs</b></h1>
                  <p class="title-description"><hr/></p>
                    <a href="{{url('tab')}}/create/{{$event->id}}" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-plus"></em> Add</a>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-block">
                    <div class="card-title-block">
                    </div>
                    <section class="example">
                      <table class="table table_noxus table-hover">
                        <thead>
                          <tr>
                            <th>Number</th>
                            <th>Title</th>
                            <th>Slug</th>
                            <th>Status</th>
                            <th>Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $n = 1;  ?>
                          @foreach($tabs as $tab)
                          <tr>
                            <th>{{$n++}}</th>
                            <th>{{@$tab['title']}}</th>
                            <th>{{@$tab['slug']}}</th>
                            <th>{{@$tab['status'] ? "Published" : "Unpublished" }}</th>
                            <th width="150px;">
                              <div class="actions_noxus">
                                @if(isset($tab['tab_id']))
                                <a class="button_left" href="{{url('get_tab')}}/{{$event->id}}/{{$tab['tab_id']}}/edit">
                                  <button type="button" class="btn btn-pill-left btn-info"><em class="fa fa-pencil-square-o"></em></button>
                                </a>
                                <form class="button_right" role="form" action="{{route('tab.destroy',[$event->id, $tab['tab_id']])}}" method="POST">
                                  {!! method_field('DELETE') !!}
                                  {!! csrf_field() !!}
                                  <button onclick="return areYouSure(event,this.form)" class="btn btn-pill-right btn-danger" type="submit"><em class="fa fa-eraser"></em></button>
                                </form>
                                @else
                                <span>Legacy Data <hr/>Not Available</span>
                                @endif
                              </div>
                            </th>
                          </tr>
                            @endforeach
                        </tbody>
                      </table>
                    </section>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
