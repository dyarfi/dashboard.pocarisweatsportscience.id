@extends('admin.master.main')

@section('content')
<div class="title-block">
  <h1 class="title"> Edit TAB</h1>
  <p class="title-description">From Event : {{$event->title}}</p>
</div>
@if(session('message'))
    <br/>
  <div class="alert alert-success">
      {{session('message')}}
  </div>
  <br/>
@endif
@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif
<section class="section">
  <div class="row sameheight-container">
    <div class="container-fluid">
        <div class="card card-block">
          <form action="{{ route('tab.store', $event->id)}}" method="POST" enctype="multipart/form-data" role="form">
            {!! csrf_field() !!}
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Title</label>
                  <input type="text" name="title" placeholder="" class="form-control underlined" value=""> 
                </div>
                <div class="form-group">
                  <label class="control-label">Columns</label>
                  <select name="container" class="selector_noxus">
                    <option value="" selected>Select Your Column Size</option>
                    <option value="full-width">Full Width</option>
                    <option value="small">Small</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Slug</label>
                  <input type="text" name="slug" placeholder="" class="form-control underlined" value=""> 
                </div>
                <div class="form-group">
                  <label class="control-label">Status</label>
                  <select name="status" class="selector_noxus">
                    <option value="" selected>Select Status</option>
                    <option value="1">Published</option>
                    <option value="0">Unpublished</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label">Content</label>
                  <div name="content" class="editor_noxus"></div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group pull-right">
                  <button type="submit" class="btn btn-info pull-right"><em class="fa fa-check-square-o"></em> Submit</button>
                  <a href="{{ route('event.edit', $event->id) }}" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-hand-o-left"></em> Cancel</a>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
@endsection