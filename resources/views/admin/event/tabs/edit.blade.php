@extends('admin.master.main')

@section('content')

<div class="title-block">
	<h1 class="title"> Edit TAB</h1>
	<p class="title-description">Title : "{{$tab['title']}}"</p>
</div>
@if(session('message'))
		<br/>
	<div class="alert alert-success">
			{{session('message')}}
	</div>
	<br/>
@endif
@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif
<section class="section">
	<div class="row sameheight-container">
		<div class="container-fluid">
				<div class="card card-block">
          <form action="{{ route('tab.update', [$event->id, $tab['tab_id']]) }}" id="submit-tab" method="POST" enctype="multipart/form-data" role="form">
						@if (@$event->id)
						{!! method_field('POST') !!}
						@endif
            {!! csrf_field() !!}
            <input type="hidden" name="type" value="{{$tab['type']}}">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Title</label>
									<input type="text" name="title" placeholder="" class="form-control underlined" value="{{ old('title',$tab['title'])}}">
								</div>
								<div class="form-group">
									<label class="control-label">Columns</label>
									<select name="container" class="selector_noxus">
										<option value="{{old('container',@$tab['container'])}}" selected>{{@$tab['container']}}</option>
										<option value="full-width">Full Width</option>
										<option value="small">Small</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Slug</label>
									<input type="text" name="slug" placeholder="" class="form-control underlined" value="{{ old('title',$tab['slug'])}}">
								</div>
								<div class="form-group">
									<label class="control-label">Status</label>
									<select name="status" class="selector_noxus">
										<option value="{{old('status',$tab['status'])}}" selected>{{$tab['status'] ? "Published" : "Unpublished"}}</option>
										<option value="1">Published</option>
										<option value="0">Unpublished</option>
									</select>
								</div>
							</div>
						</div>

            @if($tab['type']=='b')
            @if($tab['schedule_content'])
						<div class="container">
              <input type="hidden" name="tab_id" value="{{$tab['tab_id']}}">
							<div class="row">
                  <?php
                  $b = 0;
                  $c = 0;
                  ?>
							  	@foreach ($tab['schedule_content'] as $item)
                    <div class="col-md-4 my-3 py-2 bg-light">
                      <div class="form-group">
                          @foreach ($item as $key => $row)
                          <?php
                            $html = '<label class="control-label">'.ucwords(str_replace('_',' ', $key)).'</label>
                          <input type="text" name="schedule_content['.$b.']['.$key.']" placeholder="" class="form-control underlined" value="'.old('schedule_content['.$b.']['.$key.']',$row).'" data-rel="schedule_content['.$b.']['.$key.']">';
                            // Start flagged
                            if ($key == 'match_date') {
                            $row = $row->toDateTime()->format('m/d/Y h:i A');
                            $html = '<label class="control-label">'.ucwords(str_replace('_',' ', $key)).'</label>'
                            .'<div class="float-right">Delete <input name="deleted" value="1" type="checkbox"></div>'
                            .'<div class="col-md-10 row"><div class="input-group schedule_content['.$b.']['.$key.']" id="datetimepicker'.$b.'" data-target-input="nearest">
                                <input name="schedule_content['.$b.']['.$key.']" value="'.$row.'" data-rel="schedule_content['.$b.']['.$key.']" class="form-control datetimepicker-input underlined" data-target="#datetimepicker'.$b.'">
                                <span class="input-group-addon" data-target="#datetimepicker'.$b.'" data-toggle="datetimepicker">
                                <span class="fa fa-calendar"></span>
                                </span>
                              </div></div>';
                            }
                            ?>
                            {!! $html !!}
                          @endforeach
                      </div>
                    </div>
                    <?php
                    $b++;
                    $c = $b;
                    ?>
								  @endforeach
									<div class="col-md-12 my-5">
                    <div class="title-block">
                      <h3 class="title my-3">Add New Match</h3>
                    </div>
                    <div class="form-group">
                      <div id="submit-new">
                        <div class="form-row">
                          <div class="col-md-8">
                            <label class="control-label">Match Date</label>
                            <div class="input-group schedule_content[{{$c}}][match_date]" id="datetimepicker{{$c}}" data-target-input="nearest">
                              <input name="schedule_content[{{$c}}][match_date]" value="{{old('schedule_content['.$c.'][match_date]')}}" class="form-control datetimepicker-input underlined" data-target="#datetimepicker{{$c}}">
                              <span class="input-group-addon" data-target="#datetimepicker{{$c}}" data-toggle="datetimepicker">
                              <span class="fa fa-calendar"></span>
                              </span>
                            </div>
                          </div>
                        </div>
                        <label class="control-label">Match Time</label>
                        <input name="schedule_content[{{$c}}][match_time]" placeholder="19.40 - 20.25" class="form-control underlined" value="{{old('schedule_content['.$c.'][match_time]')}}"
                        type="text">
                        <label class="control-label">Round</label>
                        <input name="schedule_content[{{$c}}][round]" placeholder="Final" class="form-control underlined" value="{{old('schedule_content['.$c.'][round]')}}" type="text">
                        <label class="control-label">First Team</label>
                        <input name="schedule_content[{{$c}}][first_team]" placeholder="MANUTD" class="form-control underlined" value="{{old('schedule_content['.$c.'][first_team]')}}" type="text">
                        <label class="control-label">First Team Score</label>
                        <input name="schedule_content[{{$c}}][first_team_score]" placeholder="0" class="form-control underlined" value="{{old('schedule_content['.$c.'][first_team_score]')}}" type="text">
                        <label class="control-label">Second Team</label>
                        <input name="schedule_content[{{$c}}][second_team]" placeholder="MANCITY" class="form-control underlined" value="{{old('schedule_content['.$c.'][second_team]')}}" type="text">
                        <label class="control-label">Second Team Score</label>
                        <input name="schedule_content[{{$c}}][second_team_score]" placeholder="0" class="form-control underlined" value="{{old('schedule_content['.$c.'][second_team_score]')}}" type="text">
                        <label class="control-label">Venue</label>
                        <input name="schedule_content[{{$c}}][venue]" placeholder="SUGBK" class="form-control underlined" value="{{old('schedule_content['.$c.'][venue]')}}" type="text">
                        <label class="control-label">Live</label>
                        <input name="schedule_content[{{$c}}][live]" placeholder="0" class="form-control underlined" value="{{old('schedule_content['.$c.'][live]')}}" type="text">
                      </div>
                    </div>  
									</div>
							</div>
            </div>
            @else
            <div class="col-md-12 my-5">
                <div class="title-block">
                  <h3 class="title my-3">Add New Match</h3>
                </div>
                <input type="hidden" name="tab_id" value="{{$tab['tab_id']}}">
                <div class="form-group">
                  <div id="submit-new">
                    <div class="form-row">
                      <div class="col-md-8">
                        <label class="control-label">Match Date</label>
                        <div class="input-group schedule_content[0][match_date]" id="datetimepicker0" data-target-input="nearest">
                          <input name="schedule_content[0][match_date]" value="{{old('schedule_content[0][match_date]')}}" class="form-control datetimepicker-input underlined" data-target="#datetimepicker0">
                          <span class="input-group-addon" data-target="#datetimepicker0" data-toggle="datetimepicker">
                          <span class="fa fa-calendar"></span>
                          </span>
                        </div>
                      </div>
                    </div>
                    <label class="control-label">Match Time</label>
                    <input name="schedule_content[0][match_time]" placeholder="19.40 - 20.25" class="form-control underlined" value="{{old('schedule_content[0][match_time]')}}"
                    type="text">
                    <label class="control-label">Round</label>
                    <input name="schedule_content[0][round]" placeholder="Final" class="form-control underlined" value="{{old('schedule_content[0][round]')}}" type="text">
                    <label class="control-label">First Team</label>
                    <input name="schedule_content[0][first_team]" placeholder="MANUTD" class="form-control underlined" value="{{old('schedule_content[0][first_team]')}}" type="text">
                    <label class="control-label">First Team Score</label>
                    <input name="schedule_content[0][first_team_score]" placeholder="0" class="form-control underlined" value="{{old('schedule_content[0][first_team_score]')}}" type="text">
                    <label class="control-label">Second Team</label>
                    <input name="schedule_content[0][second_team]" placeholder="MANCITY" class="form-control underlined" value="{{old('schedule_content[0][second_team]')}}" type="text">
                    <label class="control-label">Second Team Score</label>
                    <input name="schedule_content[0][second_team_score]" placeholder="0" class="form-control underlined" value="{{old('schedule_content[0][second_team_score]')}}" type="text">
                    <label class="control-label">Venue</label>
                    <input name="schedule_content[0][venue]" placeholder="SUGBK" class="form-control underlined" value="{{old('schedule_content[0][venue]')}}" type="text">
                    <label class="control-label">Live</label>
                    <input name="schedule_content[0][live]" placeholder="0" class="form-control underlined" value="{{old('schedule_content[0][live]')}}" type="text">
                  </div>
                </div>  
              </div>
            @endif
            @else

						<div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label">Content</label>
                  <div name="content" class="editor_noxus">{!! old('content',@$tab['body_content']) !!}</div>
                </div>
              </div>
            </div>

						@endif
						<div class="row">
							<div class="col-md-12">
								<div class="form-group pull-right">
									<button type="submit" class="btn btn-info pull-right"><em class="fa fa-check-square-o"></em> Submit</button>
									<a href="{{ route('event.edit', $event->id) }}" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-hand-o-left"></em> Cancel</a>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
@endsection
