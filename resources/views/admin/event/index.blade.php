@extends('admin.master.main')

@section('content')
<div class="title-block">
	<h1 class="title"> Event </h1>
	<p class="title-description"> Index Table Event </p>
	<a href="{{ route('event.create')}}" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-plus"></em> Add</a>
</div>

@if(session('message'))
  <div class="alert alert-success">
	  {{session('message')}}
  </div>
@endif
<section class="section">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-block">
					<div class="card-title-block">
					</div>
					<section class="example">
						<table class="table table_noxus table-hover">
							<thead>
								<tr>
									<th>Thumbnail</th>
									<th>Title</th>
									<th>Type</th>
									<th>Status</th>
									<th>Date</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($events as $event)
								<tr>
									<th><img class="thumb_noxs" src="{{asset('uploads')}}/{{$event->image_thumbnail}}"/></th>
									<th>{{$event->title}}</th>
									<th>{{$event->type}}</th>
									<th>{{$event->status ? "Published" : "Unpublished" }}</th>
									<th>
										@if($event->date)
										<?php 
											$utcdatetime = $event->date;
										  $datetime = $utcdatetime->toDateTime();
										  $time=$datetime->format('m/d/Y H:m:s');
										?>
										{{$time }}
										@endif
									</th>
									<th width="150px;">
										<div class="actions_noxus">
											<a class="button_left" href="{{url('event')}}/{{$event->id}}/edit">
												<button type="button" class="btn btn-pill-left btn-info"><em class="fa fa-pencil-square-o"></em></button>
											</a>
					          	<form class="button_right" role="form" action="{{ route ('event.destroy', $event->id) }}" method="POST">
				            		{!! method_field('DELETE') !!}
					              {!! csrf_field() !!}
										  	<button onclick="return areYouSure(event,this.form)" class="btn btn-pill-right btn-danger" type="submit"><em class="fa fa-eraser"></em></button>
				            	</form>
				            </div>
									</th>
								</tr>
									@endforeach
							</tbody>
						</table>
					</section>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection