@extends('admin.master.main')

@section('content')
<div class="title-block">
  <h1 class="title"> Edit Event</h1>
  <p class="title-description">Titled : {{$event->title}}</p>
  <span>Total Tab : {{$count_tabs}}</span>
</div>
@if(session('message'))
    <br/>
  <div class="alert alert-success">
      {{session('message')}}
  </div>
  <br/>
@endif
@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif
<section class="section">
    <ul class="nav nav-tabs nav-tabs-bordered">
      <li class="nav-item">
        <a href="#home" class="nav-link active" data-target="#home" data-toggle="tab" aria-controls="home" role="tab" aria-expanded="true">Event</a>
      </li>
      <li class="nav-item">
        <a href="#tab-index" class="nav-link" data-target="#tab-index" aria-controls="tab-index" data-toggle="tab" role="tab" aria-expanded="false">Tab Index</a>
      </li>
    </ul>
</section>
<!-- Tab panes -->
<div class="tab-content tabs-bordered">
  <div class="tab-pane fade in active show" id="home" aria-expanded="true">
    <br/>
    @include('admin.event.tabs.event_information')
  </div>
    @include('admin.event.tabs.index')
</div>

  
@endsection