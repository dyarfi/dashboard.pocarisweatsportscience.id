@include('admin.master.header')
<div class="auth">
  <div class="auth-container">
    <div class="card">
      <header class="auth-header">
        <h1 class="auth-title">
          <div class="logo">
            <img src="{{asset('logo.png')}}"/>
          </div>
        </h1>
        </header>
        <div class="auth-content">
          <p class="text-center">DASHBOARD</p>
            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="username">Username</label>
              <input type="text" class="form-control underlined" name="username" id="username" placeholder="Input your username" required> 
              @if ($errors->has('email'))
                <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
                </span>
              @endif
            </div>
              <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control underlined" name="password" id="password" placeholder="Your password" required>
                @if ($errors->has('password'))
                <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
              </div>
              <br/>
              <div class="form-group">
                <button type="submit" class="btn btn-block btn-primary">Login</button>
              </div>
              </form>
            </div>
          </div>
        </div>
        </div>
@include('admin.master.footer')