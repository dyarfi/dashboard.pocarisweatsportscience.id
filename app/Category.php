<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Category extends Model 
{
  protected $table = 'categories';

  protected $fillable = [
  	'name','alias','order_number','status','type'
  ];
}
