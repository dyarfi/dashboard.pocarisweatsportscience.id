<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestNewEvent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
          'image' => 'required|image|max:250|file',
          'image_thumbnail' => 'required|image|max:250|file',
          'image_background' => 'required|image|max:250|file',
          'image_mobile' => 'required|image|max:250|file',
          'video' => '',
          'register' => '',
          'date' => 'required',
          'title' => 'required',
          'content' => 'required',
          'status' => 'boolean',
          'tag' => 'required',
          'content_tag_article' => 'required',
          'content_tag_gallery' => 'required'
        ];
        $rules['mce_0'] = 'required';

        return $rules;
    }
  protected function formatErrors (Validator $validator)
  {
      return $validator->errors()->all();
  }
}
