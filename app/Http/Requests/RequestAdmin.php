<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestAdmin extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:255',
            'email' => 'required|unique:admin_users|email',
            'password' => 'required',
            'username' => 'required|unique:admin_users|min:3|max:30'
        ];
    }

    protected function formatErrors (Validator $validator)
    {
        return $validator->errors()->all();
    }
}
