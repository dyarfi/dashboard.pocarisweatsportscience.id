<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestCategory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'name' => 'min:4|max:255|regex:/^[\pL\s\-]+$/u',
            // 'alias' => 'min:4|max:255|regex:/^[\pL\s\-]+$/u',
            // 'type' => 'min:4|max:255|regex:/^[\pL\s\-]+$/u',
            'name' => 'min:4|max:255',
            'alias' => 'max:255',
            'type' => 'min:4|max:255',
            'status' => 'boolean',
            'order_number' =>'integer'
        ];
    }

    protected function formatErrors (Validator $validator)
    {
        return $validator->errors()->all();
    }
}
