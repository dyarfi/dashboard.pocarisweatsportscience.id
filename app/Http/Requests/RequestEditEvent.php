<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestEditEvent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'image' => 'image|max:250|file',
          'image_thumbnail' => 'image|max:250|file',
          'image_background' => 'image|max:250|file',
          'image_mobile' => 'image|max:250|file',
          'video' => '',
          'register' => '',
          'date' => '',
          'title' => 'required',
          'content' => 'required',
          'status' => 'boolean',
          'tag' => 'required',
          'content_tag_article' => 'required',
          'content_tag_gallery' => 'required'
        ];
    }
    protected function formatErrors (Validator $validator)
    {
        return $validator->errors()->all();
    }
}
