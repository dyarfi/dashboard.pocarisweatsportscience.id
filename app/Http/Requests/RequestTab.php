<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestTab extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      $rules = [];

      $rules['title'] = '';
      $rules['slug'] = '';
      $rules['mce_0'] = '';
      $rules['status'] = '';
      $rules['columns'] = '';

      return $rules;
    }
  protected function formatErrors (Validator $validator)
  {
      return $validator->errors()->all();
  }
}
