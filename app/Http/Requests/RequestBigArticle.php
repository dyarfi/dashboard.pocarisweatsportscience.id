<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestBigArticle extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'header_type' => 'required',
            'title' => 'required',
            'slug' => 'required',
            //'content' => 'required',
            'status' => 'boolean',
            //'image' => 'required',
            //'image_mobile' => 'required',
        ];

        return $rules;
    }
}
