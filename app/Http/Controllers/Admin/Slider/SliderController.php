<?php

namespace App\Http\Controllers\Admin\Slider;

use Illuminate\Http\Request;
use App\Slider;
use File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;

use App\Http\Requests\RequestSlider;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::all();
        return view('admin.slider.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestSlider $request)
    {
      $inputs = array_except(Input::all(),['_method','_token']);
      $year = date('Y', time());
      $month = date('m', time());

      if($request->hasFile('image')){
        $image = $request->file('image');
        $name = $image->getClientOriginalName();
        $format_folder = $year .'/'.  $month;
        $image_store = $image->storeAs($format_folder . '/', $name , 'pocarisweat');
        $inputs['image'] =  $format_folder . '/' . $name;
      }
      if($request->hasFile('image_mobile')){
        $image_mobile = $request->file('image_mobile');
        $name_mobile = $image_mobile->getClientOriginalName();
        $format_folder = $year .'/'.  $month;
        $image_store_2 = $image_mobile->storeAs($format_folder . '/', $name_mobile , 'pocarisweat');
        $inputs['image_mobile'] =  $format_folder . '/' . $name_mobile;
      }
      Slider::create($inputs);
      return Redirect::route('slider.index')->with('message', 'Successful Add New Slider');
    }

    public function edit($id)
    {
        $slider = Slider::where('_id', $id)->first();
        return view('admin.slider.edit', compact('slider'));
    }
    public function update(RequestSlider $request, $id)
    {
      $slider = Slider::where('_id', '=',$id)->first();
      $inputs = array_except(Input::all(),['_method','_token']);
      $year = date('Y', time());
      $month = date('m', time());

      if($request->hasFile('image')){
        if($slider->image){
          $old_image = $slider->image;
          if($old_image){
            $slider = Slider::findOrFail($id);
            #File::delete('../../api/public/uploads/' . $slider->image);
          }
        }
        $image = $request->file('image');
        $name = $image->getClientOriginalName();
        $format_folder = $year .'/'.  $month;
        $image_store = $image->storeAs($format_folder . '/', $name , 'pocarisweat');
        $inputs['image'] =  $format_folder . '/' . $name;
      }

      if($request->hasFile('image_mobile')){
        if($slider->image_mobile){
          $old_image_2 = $slider->image_mobile;
          if($old_image_2){
            $slider = Slider::findOrFail($id);
            #File::delete('../../api/public/uploads/' . $slider->image_mobile);
          }
        }
        $image_mobile = $request->file('image_mobile');
        $name_mobile = $image_mobile->getClientOriginalName();
        $format_folder = $year .'/'.  $month;
        $image_store_2 = $image_mobile->storeAs($format_folder . '/', $name_mobile , 'pocarisweat');
        $inputs['image_mobile'] =  $format_folder . '/' . $name_mobile;
      }
      $slider->update($inputs);
      return Redirect::route('slider.index')->with('message', 'Successful Edit Slider Image');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Slider::findOrFail($id);
        #File::delete('../../api/public/uploads/' . $slider->image_mobile);
        #File::delete('../../api/public/uploads/' . $slider->image);
        Slider::destroy($id);
        return Redirect::route('slider.index')->with('message', 'Successful Delete Slider');
    }
}
