<?php

namespace App\Http\Controllers\Admin\Article;
use Illuminate\Http\Request;
use App\Article;
use App\Category;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use File;
use Illuminate\Support\Facades\Input;
use MongoDB\BSON\UTCDateTime;

use App\Http\Requests\RequestArticle;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::getTypeArticle();
        return view('admin.article.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.article.add',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestArticle $request)
    {
        $inputs = Input::all();
        $year = date('Y', time());
        $month = date('m', time());
        if($request->date){
        $milliseconds_date = strtotime($request->date);
        $inputs['date'] = new \MongoDB\BSON\UTCDateTime($milliseconds_date *1000);
        }
        $inputs['content'] = $request->mce_0;
        $inputs['type'] = 'article';
        if($request->hasFile('image')){
          $image = $request->file('image');
          $name = $image->getClientOriginalName();
          $format_folder = $year .'/'.  $month;
          $image_store = $image->storeAs($format_folder . '/', $name , 'pocarisweat');
          $inputs['image'] =  $format_folder . '/' . $name;
        }
        Article::create($inputs);
        return Redirect::route('article.index')->with('message', 'Successful Add New Article');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::where('_id','=', $id)->first();
        $utcdatetime = $article->date;
        $datetime = $utcdatetime->toDateTime();
        $time = $datetime->format('m/d/Y H:m:s');
        $article->date = $time;
        $categories = Category::all();
        return view('admin.article.edit',compact('article','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RequestArticle $request, $id)
    {
        $article = Article::where('_id', '=',$id)->first();
        $inputs = Input::all();
        $year = date('Y', time());
        $month = date('m', time());
        if($request->date){
        $milliseconds_date = strtotime($request->date);
        $inputs['date'] = new \MongoDB\BSON\UTCDateTime($milliseconds_date *1000);
        }
        $inputs['content'] = $request->mce_0;
        if($request->hasFile('image')){
          if($article->image){
            $old_image = $article->image;
            if($old_image){
              $article = Article::findOrFail($id);
              #File::delete('../../api/public/uploads/' . $article->image);
            }
          }
          $image = $request->file('image');
          $name = $image->getClientOriginalName();
          $format_folder = $year .'/'.  $month;
          $image_store = $image->storeAs($format_folder . '/', $name , 'pocarisweat');
          $inputs['image'] =  $format_folder . '/' . $name;
        }
        $article->update($inputs);
        return Redirect::route('article.index')->with('message', 'Successful Edit Article : ' . $article->title);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::where('_id', '=',$id)->first();
        if($article->image){
          $old_image = $article->image;
          if($old_image){
            $article = Article::findOrFail($id);
            #File::delete('../../api/public/uploads/' . $article->image);
          }
        }        
        Article::destroy($id);
        return Redirect::route('article.index')->with('message', 'Successful Delete Article');
    }
}
