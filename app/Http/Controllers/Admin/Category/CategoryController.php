<?php

namespace App\Http\Controllers\Admin\Category;

use Illuminate\Http\Request;
use App\Category;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use App\Http\Requests\RequestCategory;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $categories = Category::all();
      return view('admin.category.index', compact('categories'));
    }

    public function create()
    {
      return view('admin.category.add');
    }

    public function store(RequestCategory $request)
    {
      $inputs= Input::all();
      Category::create($inputs);
      return Redirect::route('category.index')->with('message','Successful Add New Category');
    }

    public function edit($id)
    {
      $category = Category::where('_id',$id)->first();
      return view('admin.category.edit',compact('category'));
    }


    public function update(RequestCategory $request, $id)
    {
      $category = Category::where('_id', '=',$id);
      $inputs = array_except(Input::all(),['_method','_token']);
      $category->update($inputs);
      return Redirect::route('category.index')->with('message', 'Successful Update Category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Category::destroy($id);
      return Redirect::route('category.index')->with('message', 'Successful Delete Category');
    }
  }
