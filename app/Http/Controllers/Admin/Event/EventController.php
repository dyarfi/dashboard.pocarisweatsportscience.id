<?php

namespace App\Http\Controllers\Admin\Event;

use Illuminate\Http\Request;
use App\Event;
use App\Category;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use File;
use Session;
use Illuminate\Support\Facades\Input;
use MongoDB\BSON\UTCDateTime;

use App\Http\Requests\RequestNewEvent;
use App\Http\Requests\RequestEditEvent;


class EventController extends Controller
{
    public function index()
    {
        $events = Event::getTypeEvent();
        return view('admin.event.index',compact('events'));

    }

    public function create()
    {
        $categories = Category::all();
        return view('admin.event.add',compact('categories'));
    }

    public function store(RequestNewEvent $request)
    {
        $inputs = Input::all();
        $year = date('Y', time());
        $month = date('m', time());
        if($request->date){
        $milliseconds_date = strtotime($request->date);
        $inputs['date'] = new \MongoDB\BSON\UTCDateTime($milliseconds_date *1000);
        }
        if($request->hasFile('image')){
          $image = $request->file('image');
          $name = $image->getClientOriginalName();
          $format_folder = $year .'/'.  $month;
          $image_store = $image->storeAs($format_folder . '/', $name , 'pocarisweat');
          $inputs['image'] =  $format_folder . '/' . $name;
        }
        if($request->hasFile('image_thumbnail')){
          $image_thumbnail = $request->file('image_thumbnail');
          $name = $image_thumbnail->getClientOriginalName();
          $format_folder = $year .'/'.  $month;
          $image_store = $image_thumbnail->storeAs($format_folder . '/', $name , 'pocarisweat');
          $inputs['image_thumbnail'] =  $format_folder . '/' . $name;
        }
        if($request->hasFile('image_background')){
          $image_background = $request->file('image_background');
          $name = $image_background->getClientOriginalName();
          $format_folder = $year .'/'.  $month;
          $image_store = $image_background->storeAs($format_folder . '/', $name , 'pocarisweat');
          $inputs['image_background'] =  $format_folder . '/' . $name;
        }
        if($request->hasFile('image_mobile')){
          $image_background = $request->file('image_mobile');
          $name = $image_background->getClientOriginalName();
          $format_folder = $year .'/'.  $month;
          $image_store = $image_background->storeAs($format_folder . '/', $name , 'pocarisweat');
          $inputs['image_mobile'] =  $format_folder . '/' . $name;
        }

        $inputs['content']['body_content'] = $request->mce_0;
        $inputs['category'] = 'Event';
        $inputs['writer']['name'] = 'POCARI SWEAT';
        $inputs['type'] = 'event';
        $inputs['content'] = array($request->content);
        // Override event_date and countdown data
        $inputs['content'][0]['event_date'] = new \MongoDB\BSON\UTCDateTime(strtotime($request->content['event_date']) *1000);
        $inputs['content'][0]['countdown']['title'] = @$request->content['countdown']['title'];
        $inputs['content'][0]['countdown']['btn_title'] = @$request->content['countdown']['btn_title'];
        $inputs['content'][0]['countdown']['btn_class'] = @$request->content['countdown']['btn_class'];
        $inputs['content'][0]['countdown']['btn_url'] = @$request->content['countdown']['btn_url'];
        $inputs['content'][0]['countdown']['countdown_date'] = new \MongoDB\BSON\UTCDateTime(strtotime($request->content['countdown']['countdown_date']) *1000);
        //$inputs['content'][0]['countdown']['countdown_date'] = @$request->content['countdown']['countdown_date'];

        $inputs['content']['0']['body_content'] = $inputs['mce_0'];
        $inputs['content']['0']['title'] = "INFORMATION";
        $inputs['content']['0']['type'] = 'a';
        $inputs['content']['0']['tab_id'] = 'noxus_'.str_random(10);
        $inputs['content']['0']['slug'] = "information";
        // Sidebars
        $sidebars['list'] = [];
        foreach ($request->content['left_sidebar']['list'] as $n => $value) {
          if ($n == 3) {
            $value['value'] = $request->mce_1;
          }
          if ($n == 4) {
            $value['value'] = $request->mce_2;
          }
          $sidebars['list'][$n] = $value;
        }
        $_sidebar = array_replace($request->content['left_sidebar'], $sidebars);
        $inputs['content']['0']['left_sidebar'] = array($_sidebar);

        $inputs['content']['1']['title'] = "ARTICLES";
        $inputs['content']['1']['slug'] = "articles";
        $inputs['content']['1']['type'] = "c";
        $inputs['content']['1']['content_tag'] = $inputs['content_tag_article'];
        $inputs['content']['1']['status'] = '1';
        $inputs['content']['1']['tab_id'] = 'noxus_'.str_random(10);

        $inputs['content']['2']['title'] = "GALLERY";
        $inputs['content']['2']['slug'] = "gallery";
        $inputs['content']['2']['type'] = "c";
        $inputs['content']['2']['content_tag'] = $inputs['content_tag_gallery'];
        $inputs['content']['2']['status'] = '1';
        $inputs['content']['2']['tab_id'] = 'noxus_'.str_random(10);

        $inputs['tag'] = $request->tag;

        Event::create($inputs);
        return Redirect::route('event.index')->with('message', 'Successful Add New Event | Edit Your Event For Adding New Tab');
    }

    public function show($id)
    {

    }


    public function edit($id)
    {
        $event = Event::where('_id','=', $id)->first();
        $utcdatetime = $event->date;
        $datetime = $utcdatetime->toDateTime();
        $time = $datetime->format('m/d/Y H:m:s');
        $event->date = $time;
        $information = $event->content[0];
        $article = $event->content[1];
        $gallery = $event->content[2];

        $tabs = array_slice($event->content,3);
        $count_tabs = count($event->content);

        return view('admin.event.edit',compact('event','information','tabs','count_tabs','article','gallery'));
    }

    public function update(RequestEditEvent $request, $id)
    {
        // dd($request->content['left_sidebar']);
        $event = Event::where('_id', '=',$id)->first();
        $inputs = Input::all();
        $year = date('Y', time());
        $month = date('m', time());
        if($request->hasFile('image')){
          if($event->image){
            $old_image = $event->image;
            if($old_image){
              $event = Event::findOrFail($id);
              #File::delete('../../api/public/uploads/' . $event->image);
            }
          }
          $image = $request->file('image');
          $name = $image->getClientOriginalName();
          $format_folder = $year .'/'.  $month;
          $image_store = $image->storeAs($format_folder . '/', $name , 'pocarisweat');
          $inputs['image'] =  $format_folder . '/' . $name;
        } else {
          $inputs['image'] = $event->image;
        }

        if($request->hasFile('image_thumbnail')){
          if($event->image_thumbnail){
            $old_image = $event->image_thumbnail;
            if($old_image){
              $event = Event::findOrFail($id);
              #File::delete('../../api/public/uploads/' . $event->image_thumbnail);
            }
          }
          $image = $request->file('image_thumbnail');
          $name = $image->getClientOriginalName();
          $format_folder = $year .'/'.  $month;
          $image_store = $image->storeAs($format_folder . '/', $name , 'pocarisweat');
          $inputs['image_thumbnail'] =  $format_folder . '/' . $name;
        } else {
          $inputs['image_thumbnail'] = $event->image_thumbnail;
        }

        if($request->hasFile('image_background')){
          if($event->image_background){
            $old_image = $event->image_background;
            if($old_image){
              $event = Event::findOrFail($id);
              #File::delete('../../api/public/uploads/' . $event->image_background);
            }
          }
          $image = $request->file('image_background');
          $name = $image->getClientOriginalName();
          $format_folder = $year .'/'.  $month;
          $image_store = $image->storeAs($format_folder . '/', $name , 'pocarisweat');
          $inputs['image_background'] =  $format_folder . '/' . $name;
        } else {
          $inputs['image_background'] = $event->image_background;
        }

        if($request->hasFile('image_mobile')){
          if($event->image_background){
            $old_image = $event->image_background;
            if($old_image){
              $event = Event::findOrFail($id);
              #File::delete('../../api/public/uploads/' . $event->image_mobile);
            }
          }
          $image = $request->file('image_mobile');
          $name = $image->getClientOriginalName();
          $format_folder = $year .'/'.  $month;
          $image_store = $image->storeAs($format_folder . '/', $name , 'pocarisweat');
          $inputs['image_mobile'] =  $format_folder . '/' . $name;
        } else {
          $inputs['image_mobile'] = $event->image_mobile;
        }
        $inputs['content']['body_content'] = $request->mce_0;
        $inputs['category'] = 'Event';
        $inputs['writer']['name'] = 'POCARI SWEAT';
        $inputs['type'] = 'event';
        $inputs['content'] = $event->content;
        // Override event_date and countdown data
        //dd($request->content['countdown']);
        $inputs['content'][0]['event_date'] = new \MongoDB\BSON\UTCDateTime(strtotime($request->content['event_date']) *1000);
        $inputs['content'][0]['countdown']['title'] = @$request->content['countdown']['title'];
        $inputs['content'][0]['countdown']['btn_title'] = @$request->content['countdown']['btn_title'];
        $inputs['content'][0]['countdown']['btn_class'] = @$request->content['countdown']['btn_class'];
        $inputs['content'][0]['countdown']['btn_url'] = @$request->content['countdown']['btn_url'];
        $inputs['content'][0]['countdown']['countdown_date'] = new \MongoDB\BSON\UTCDateTime(strtotime($request->content['countdown']['countdown_date']) *1000);
        //$inputs['content'][0]['countdown']['countdown_date'] = @$request->content['countdown']['countdown_date'];
        //04/02/2018 5:46 PM
        $inputs['content']['0']['body_content'] = $inputs['mce_0'];
        $inputs['tag'] = $request->tag;
        // Sidebars
        $sidebars['list'] = [];
        foreach ($request->content['left_sidebar']['list'] as $n => $value) {
          if ($n == 3) {
            $value['value'] = $request->mce_1;
          }
          if ($n == 4) {
            $value['value'] = $request->mce_2;
          }
          $sidebars['list'][$n] = $value;
        }
        $_sidebar = array_replace($request->content['left_sidebar'], $sidebars);
        $inputs['content']['0']['left_sidebar'] = array($_sidebar);
        $inputs['date'] = $event->date;
        $inputs['content']['0']['body_button']= $request->content['body_button'];

        $inputs['content']['1']['content_tag'] = $inputs['content_tag_article'];
        $inputs['content']['2']['content_tag'] = $inputs['content_tag_gallery'];

        $event->update($inputs);
        return Redirect::route('event.index')->with('message', 'Success Edit Event');
    }

    public function destroy($id)
    {
        $event = Event::findOrFail($id);
        return $event['image'] . $event['image_thumbnail'] . $event['image_background'];
        if($event->image){
          $old_image = $article->image;
          if($old_image){
            #File::delete('../../api/public/uploads/' . $event->image);
          }
        }
        if($event->image_thumbnail){
          $old_image = $event->image_thumbnail;
          if($old_image){
            #File::delete('../../api/public/uploads/' . $event->image_thumbnail);
          }
        }
        if($event->image_background){
          $old_image = $event->image_thumbnail;
          if($old_image){
            #File::delete('../../api/public/uploads/' . $event->image_background);
          }
        }
        Event::destroy($id);
        return Redirect::route('event.index')->with('message', 'Successful Delete Event');
    }
}
