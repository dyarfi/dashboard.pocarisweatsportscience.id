<?php

namespace App\Http\Controllers\Admin\Event\Tab;

use Illuminate\Http\Request;
use App\Event;
use App\Category;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use File;
use Session;
use Illuminate\Support\Facades\Input;
use MongoDB\BSON\UTCDateTime;

use App\Http\Requests\RequestTab;


class TabController extends Controller
{

    public function create($event_id)
    {
      $event = Event::findOrFail($event_id);
      return view('admin.event.tabs.add',compact('event'));
    }


    public function store(RequestTab $request, $event_id)
    {
        $event = Event::findOrFail($event_id);
        return $request;
        $tab = $request->except('_token','mce_0');
        $tab['type'] = 'd';
        $tab['body_content'] = $request->mce_0;
        $tab['tab_id'] = 'noxus_' . str_random(9);
        $event->push('content', array($tab));

        $information = $event->content[0];
        $article = $event->content[1];
        $gallery = $event->content[2];
        $tabs = array_slice($event->content,3);
        $count_tabs = count($event->content);
        return Redirect::route('event.edit',compact('event','information','tabs','count_tabs','article','gallery'))->with('message', 'Successful Add New Tab : ' . $tab['title']  );
    }

    public function show($id)
    {
    }

    public function edit($id, $tab_id)
    {
    $event = Event::findOrFail($id);
    $tabs = $event->content;
      foreach ($tabs as $key => $tab) {
        if(isset($tab['tab_id']) && $event->_id == $id){
          if($tab['tab_id'] == $tab_id){
            $tab; break;
          }
        }
      }
      return view('admin.event.tabs.edit',compact('tab','event'));
    }

    public function update(RequestTab $request, $id, $tab_id)
    {
        $inputs = Input::all();

        $event = Event::findOrFail($id);
        $tabs = array_slice($event['content'],3);
        $object = collect($event)->all();

        if (@$inputs['schedule_content'] && $inputs['type'] == 'b') {
          foreach ($inputs['schedule_content'] as $input => $value) {
            $value['match_date'] = new \MongoDB\BSON\UTCDateTime(strtotime($value['match_date']) *1000);
            $inputs['schedule_content'][$input] = $value;
          }
        }

        $v=3;
        $title='';
        foreach ($tabs as $key => $tab) {
          if(isset($tab['tab_id']) && $tab['tab_id'] == $tab_id && $event['_id'] == $id){
              $title = $tab['title'];
              if ($inputs['type'] == 'b' && @$tab['schedule_content'] || @$inputs['schedule_content']) {
                $title = $tab['title'];
                $tab['schedule_content'] = collect(@$inputs['schedule_content'])->values()->all();
              }
              $tab['slug'] = $inputs['slug'];
              $tab['title'] = $inputs['title'];
              $tab['type'] = $inputs['type'];
              $tab['body_content'] = (@$inputs['mce_0']) ? $inputs['mce_0'] : '';
              $tab['status'] = $inputs['status'];
          }
          unset($object['content'][$v]);
          $object['content'][$v] = $tab;
          $v++;
        }

        $result = Event::findOrFail($id)->update($object);
        return Redirect::route('tab.get',[$id,$tab_id])->with('message', 'Successful Edit Tab : ' . $title  );
    }

    public function destroy($event_id, $tab_id)
    {

      $event = Event::findOrFail($event_id);
      // Will remark this one
      Redirect::route('event.edit',$event_id);
      // ***************************************** //
      $tabs = $event->content;
      foreach($tabs as $key => $tab){
          if(isset($tab['tab_id']) && $tab['tab_id'] == $tab_id && $event->_id == $event_id) {
            $id = $tab['tab_id'];
            $title = $tab['title'];
            unset($tabs[$key]);
            //break;
          }
      }

      $inputs['content'] = $tabs;
      $event->update($inputs);
      $information = $event->content[0];
      $tabs = array_slice($event->content,3);
      $count_tabs = count($event->content);
      return Redirect::route('event.edit',$event_id)->with('message', 'Delete Tab ');
      //return Redirect::route('event.edit',compact('event','information','tabs','count_tabs','article','gallery'))->with('message', 'Delete Tab : ' . $title  );
    }
}
