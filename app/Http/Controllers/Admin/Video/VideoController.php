<?php

namespace App\Http\Controllers\Admin\Video;

use Illuminate\Http\Request;
use App\Video;
use App\Category;
use MongoDB\BSON\UTCDateTime;
use File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;

use App\Http\Requests\RequestVideo;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = Video::getTypeVideo();
        return view('admin.video.index',compact('videos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.video.add',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestVideo $request)
    {
        $inputs = Input::all();
        $year = date('Y', time());
        $month = date('m', time());
        if($request->date){
        $milliseconds_date = strtotime($request->date);
        $inputs['date'] = new \MongoDB\BSON\UTCDateTime($milliseconds_date *1000);
        }
        $inputs['content'] = $request->mce_0;
        $inputs['type'] = 'video';
        if($request->hasFile('image')){
          $image = $request->file('image');
          $name = $image->getClientOriginalName();
          $format_folder = $year .'/'.  $month;
          $image_store = $image->storeAs($format_folder . '/', $name , 'pocarisweat');
          $inputs['image'] =  $format_folder . '/' . $name;
        }
        Video::create($inputs);
        return Redirect::route('video.index')->with('message', 'Successful Add New Video');
    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $video = Video::where('_id','=', $id)->first();
        $utcdatetime = $video->date;
        $datetime = $utcdatetime->toDateTime();
        $time = $datetime->format('m/d/Y H:m:s');
        $video->date = $time;
        $categories = Category::all();
        return view('admin.video.edit',compact('video','categories'));
    }


    public function update(RequestVideo $request, $id)
    {
        $video = Video::where('_id', '=',$id)->first();
        $inputs = Input::all();
        $year = date('Y', time());
        $month = date('m', time());
        if($request->date){
        $milliseconds_date = strtotime($request->date);
        $inputs['date'] = new \MongoDB\BSON\UTCDateTime($milliseconds_date *1000);
        }
        $inputs['content'] = $request->mce_0;
        if($request->hasFile('image')){
          if($video->image){
            $old_image = $video->image;
            if($old_image){
              $video = Video::findOrFail($id);
              #File::delete('../../api/public/uploads/' . $video->image);
            }
          }
          $image = $request->file('image');
          $name = $image->getClientOriginalName();
          $format_folder = $year .'/'.  $month;
          $image_store = $image->storeAs($format_folder . '/', $name , 'pocarisweat');
          $inputs['image'] =  $format_folder . '/' . $name;
        }
        $video->update($inputs);
        return Redirect::route('video.index')->with('message', 'Successful Edit Video : ' . $video->title);   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Video::destroy($id);
        return Redirect::route('video.index')->with('message', 'Successful Delete Video');
    }
}
