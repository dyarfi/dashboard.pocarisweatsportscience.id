<?php

namespace App\Http\Controllers\Admin\BigArticle;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BigArticle;
use App\Category;
use Illuminate\Support\Facades\Redirect;
use File;
use Illuminate\Support\Facades\Input;
use MongoDB\BSON\UTCDateTime;

use App\Http\Requests\RequestBigArticle;

class BigArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $big_articles = BigArticle::getTypeBigArticle();
        return view('admin.big_article.index', compact('big_articles'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.big_article.add',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestBigArticle $request)
    {
        $inputs = Input::all();
        $year = date('Y', time());
        $month = date('m', time());
        if($request->date){
        $milliseconds_date = strtotime($request->date);
        $inputs['date'] = new \MongoDB\BSON\UTCDateTime($milliseconds_date *1000);
        }
        $inputs['content'] = $request->mce_0;
        $inputs['type'] = 'campaign';
        $inputs['campaign'] = $request->campaign;
        if($request->hasFile('image')){
          $image = $request->file('image');
          $name = $image->getClientOriginalName();
          $format_folder = $year .'/'.  $month;
          $image_store = $image->storeAs($format_folder . '/', $name , 'pocarisweat');
          $inputs['campaign']['image'] =  $format_folder . '/' . $name;
        }
        if($request->hasFile('image_mobile')){
          $image = $request->file('image_mobile');
          $name = $image->getClientOriginalName();
          $format_folder = $year .'/'.  $month;
          $image_store = $image->storeAs($format_folder . '/', $name , 'pocarisweat');
          $inputs['campaign']['image_mobile'] =  $format_folder . '/' . $name;
        }
        // return $inputs;
        BigArticle::create($inputs);
        return Redirect::route('big_article.index')->with('message', 'Successful Add New Big Article | Campaign');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $big_article = BigArticle::where('_id','=', $id)->first();
        $utcdatetime = @$big_article->date;
        $datetime = ($utcdatetime != null) ? $utcdatetime->toDateTime() : '';
        $time = $datetime != null ? $datetime->format('m/d/Y H:m:s') : '';
        $big_article->date = @$time;
        return view('admin.big_article.edit',compact('big_article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RequestBigArticle $request, $id)
    {
        $big_article = BigArticle::where('_id','=', $id)->first();
        $inputs = Input::all();
        $year = date('Y', time());
        $month = date('m', time());
        $inputs['content'] = $request->mce_0;
        if($request->date){
        $milliseconds_date = strtotime($request->date);
        $inputs['date'] = new \MongoDB\BSON\UTCDateTime($milliseconds_date *1000);
        }
        if($request->hasFile('image')){
          if($big_article->image){
            $old_image = $big_article->image;
            if($old_image){
              $article = BigArticle::findOrFail($id);
              #File::delete('../../api/public/uploads/' . $article->image);
            }
          }
          $image = $request->file('image');
          $name = $image->getClientOriginalName();
          $format_folder = $year .'/'.  $month;
          $image_store = $image->storeAs($format_folder . '/', $name , 'pocarisweat');
          $inputs['campaign']['image'] =  $format_folder . '/' . $name;
        }
        if($request->hasFile('image_mobile')){
          if($big_article->image_mobile){
            $old_image = $big_article->image_mobile;
            if($old_image){
              $article = BigArticle::findOrFail($id);
              #File::delete('../../api/public/uploads/' . $article->image_mobile);
            }
          }
          $image = $request->file('image_mobile');
          $name = $image->getClientOriginalName();
          $format_folder = $year .'/'.  $month;
          $image_store = $image->storeAs($format_folder . '/', $name , 'pocarisweat');
          $inputs['campaign']['image_mobile'] =  $format_folder . '/' . $name;
        }
        $big_article->update($inputs);
        return Redirect::route('big_article.index')->with('message', 'Successful Edit Big Article : ' . @$big_article->campaign['title']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $big_article = BigArticle::where('_id','=', $id)->first();
        if($big_article->image){
          $old_image = $big_article->image;
          if($old_image){
            $article = Article::findOrFail($id);
            #File::delete('../../api/public/uploads/' . $article->image);
          }
        }
        if($big_article->image_mobile){
          $old_image = $big_article->image_mobile;
          if($old_image){
            $article = Article::findOrFail($id);
            #File::delete('../../api/public/uploads/' . $article->image_mobile);
          }
        }
        BigArticle::destroy($id);
        return Redirect::route('big_article.index')->with('message', 'Successful Delete Big Article');
    }
}
