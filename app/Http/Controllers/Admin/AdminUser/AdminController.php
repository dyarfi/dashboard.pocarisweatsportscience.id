<?php

namespace App\Http\Controllers\Admin\AdminUser;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;

use App\Http\Requests\RequestAdmin;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('admin.admin_user.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admin_user.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestAdmin $request)
    {
        $inputs = Input::except('password');
        $password = bcrypt($request->password);
        $inputs['password'] = $password;
        User::create($inputs);
        return Redirect::route('admin_user.index')->with('message', 'Successful Add New User : '. $inputs['name'] );
    }

    public function edit($id)
    {
        $admin = User::where('_id','=', $id)->first();
        return view('admin.admin_user.edit',compact('admin'));        
    }

    public function update(RequestAdmin $request, $id)
    {
        $admin = User::where('_id','=', $id)->first();
        $inputs = Input::except('password');
        $check_input= Input::get('password');

        if(isset($check_input)){
          $password_crypt = bcrypt($request->password);
          $inputs['password'] = $password_crypt;
        }
        $admin->update($inputs);
        return Redirect::route('admin_user.index')->with('message', 'Successful Edit User : ' . $admin->name);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return Redirect::route('admin_user.index')->with('message', 'Successful Delete Admin');
    }
}
