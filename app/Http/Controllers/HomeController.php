<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Category;
use App\Article;
use App\BigArticle;
use App\Video;
use App\Slider;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $big_articles = BigArticle::getTypeBigArticle();
        $articles = Article::getTypeArticle();
        $videos = Video::getTypeVideo();
        $sliders = Slider::all();
        $categories = Category::all();
        $events = Event::getTypeEvent();
        return view('admin.dashboard',compact('big_articles','events','articles','videos','sliders','categories'));
    }
}
