<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;
use MongoDB\BSON\UTCDateTime;

class Video extends Model 
{
  protected $table = 'articles';

  protected $fillable = [
  	'type','category','subcategory','title','caption','content','image','published_datetime','writer','status','columns','date','parent_id','tag','header_type'
  ];
  public static function getTypeVideo(){
  	return self::where('type','video')->orderBy('_id','desc')->get();
  }
}