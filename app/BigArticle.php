<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;
use MongoDB\BSON\UTCDateTime;

class BigArticle extends Model
{
    protected $table = 'articles';

    protected $fillable = ['campaign.name','campaign{name}','type','header_type','category','subcategory','title','campaign','slug','caption','content','campaign{image_mobile}','campaign{image}','published_datetime','status','date','parent_id','tag'];

    public static function getTypeBigArticle(){
    	return Article::where('type','campaign')->orderBy('_id','desc')->get();
    }
}