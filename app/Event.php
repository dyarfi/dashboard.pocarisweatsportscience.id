<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;
use MongoDB\BSON\UTCDateTime;

class Event extends Model
{
	protected $table = 'articles';

	protected $fillable = ['header_type','type','video','register','category','title','caption','content','image','image_thumbnail','image_background','image_mobile','status','columns','writer','date','tag','content_tag_article','content_tag_gallery'];
	public static function getTypeEvent(){
		return Event::where('type','event')->orderBy('_id','desc')->get();
	}
}
