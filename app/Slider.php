<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Slider extends Model 
{
  protected $table = 'slider';

  protected $fillable = [
  	'image','image_mobile','url','order_number','status'
  ];
}