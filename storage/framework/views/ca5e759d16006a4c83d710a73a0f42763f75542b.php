<?php $__env->startSection('content'); ?>
<div class="title-block">
  <h1 class="title"> Create Admin</h1>
  <p class="title-description"></p>
</div>
<?php if(session('message')): ?>
    <br/>
  <div class="alert alert-success">
      <?php echo e(session('message')); ?>

  </div>
  <br/>
<?php endif; ?>
<?php if($errors->any()): ?>
  <div class="alert alert-danger">
    <ul>
      <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <li><?php echo e($error); ?></li>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
  </div>
<?php endif; ?>
<section class="section">
  <div class="row sameheight-container">
    <div class="container-fluid">
        <div class="card card-block sameheight-item">
            <form action="<?php echo e(route('admin_user.update', $admin->id)); ?>" method="POST" enctype="multipart/form-data" role="form">
            <?php echo method_field('PUT'); ?>

            <?php echo csrf_field(); ?>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Name</label>
                  <input type="text" name="name" placeholder="" class="form-control underlined" value="<?php echo e(old('name',$admin->name)); ?>"> 
                </div>
                <div class="form-group">
                  <label class="control-label">Email</label>
                  <input type="email" name="email" placeholder="" class="form-control underlined" value="<?php echo e(old('email',$admin->email)); ?>"> 
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Username</label>
                  <input type="text" name="username" placeholder="" class="form-control underlined" value="<?php echo e(old('username',$admin->username)); ?>"> 
                </div>
                <div class="form-group">
                  <label class="control-label">Password</label>
                  <input type="password" name="password" placeholder="" class="form-control underlined" value=""> 
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-info pull-right"><em class="fa fa-check-square-o"></em> Submit</button>
            <a href="<?php echo e(route('admin_user.index')); ?>" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-hand-o-left"></em> Cancel</a>
          </form>
        </div>
      </div>
    </div>
  </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>