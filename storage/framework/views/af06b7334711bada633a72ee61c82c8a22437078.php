<?php $__env->startSection('content'); ?>
<div class="title-block">
  <h1 class="title">Edit Slider</h1>
  <p class="title-description"></p>
</div>
<?php if(session('message')): ?>
    <br/>
  <div class="alert alert-success">
      <?php echo e(session('message')); ?>

  </div>
  <br/>
<?php endif; ?>
<?php if($errors->any()): ?>
  <div class="alert alert-danger">
    <ul>
      <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <li><?php echo e($error); ?></li>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
  </div>
<?php endif; ?>
<section class="section">
  <div class="row sameheight-container">
    <div class="container-fluid">
        <div class="card card-block sameheight-item">
            <form action="<?php echo e(route('slider.update', $slider->id)); ?>" method="POST" enctype="multipart/form-data" role="form">
            <?php echo method_field('PUT'); ?>

            <?php echo csrf_field(); ?>

            <div class="row centered">
              <div class="col-md-6">
                <div class="thumb_image cont">
                  <label class="control-label"><h1>Image</h1></label>
                  <img src="<?php echo e(asset('uploads')); ?>/<?php echo e($slider->image); ?>" alt="" class="thumb_image"/>
                </div>
              </div>
              <div class="col-md-6">
                <div class="thumb_image">
                  <label class="control-label"><h1>Image Mobile</h1></label>
                  <img src="<?php echo e(asset('uploads')); ?>/<?php echo e($slider->image_mobile); ?>" alt="" class="thumb_image"/>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <hr/>
                  <span> <?php echo e($slider->image); ?></span>
                  <input type="file" name="image" placeholder="" class="form-control underlined" value="">
                </div>
                <div class="form-group">
                  <label class="control-label">Url</label>
                  <input type="text" name="url" placeholder="" class="form-control underlined" value="<?php echo e(old ('url', $slider->url)); ?>"> 
                </div>
                <div class="form-group">
                  <label class="control-label">Order Number</label>
                  <input type="text" name="order_number" placeholder="" class="form-control underlined" value="<?php echo e(old ('order_number', $slider->order_number)); ?>"> 
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <hr/>
                  <span> <?php echo e($slider->image_mobile); ?></span>
                  <input type="file" name="image_mobile" placeholder="" class="form-control underlined" value="">
                </div>
                <div class="form-group">
                  <label class="control-label">Status</label>
                  <select name="status" class="selector_noxus">
                    <option value="<?php echo e(old('status', $slider->status)); ?>" selected><?php echo e($slider->status ? "True" : "False"); ?></option>
                    <option value="1">True</option>
                    <option value="0">False</option>
                  </select>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-info pull-right"><em class="fa fa-check-square-o"></em> Submit</button>
            <a href="<?php echo e(route('slider.index')); ?>" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-hand-o-left"></em> Cancel</a>
          </form>
        </div>
      </div>
    </div>
  </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>