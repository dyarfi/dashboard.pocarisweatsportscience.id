<?php $__env->startSection('content'); ?>
<div class="title-block">
	<h1 class="title"> Category </h1>
	<p class="title-description"> Index Table Category </p>
	<a href="<?php echo e(route('category.create')); ?>" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-plus"></em> Add</a>
</div>

<?php if(session('message')): ?>
  <div class="alert alert-success">
	  <?php echo e(session('message')); ?>

  </div>
<?php endif; ?>
<section class="section">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-block">
					<div class="card-title-block">
					</div>
					<section class="example">
						<table class="table table_noxus table-hover">
							<thead>
								<tr>
									<th>Name</th>
									<th>Alias</th>
									<th>Status</th>
									<th>Type</th>
									<th>Order Number</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<tr>
									<th><?php echo e($category->name); ?></th>
									<th><?php echo e($category->alias); ?></th>
									<th><?php echo e($category->status ? "True" : "False"); ?></th>
									<th><?php echo e($category->type); ?></th>
									<th class="sort"><?php echo e($category->order_number); ?></th>
									<th>
										<div class="actions_noxus">
											<a class="button_left" href="<?php echo e(url('category')); ?>/<?php echo e($category->id); ?>/edit">
												<button type="button" class="btn btn-pill-left btn-info"><em class="fa fa-pencil-square-o"></em></button>
											</a>
					          	<form class="button_right" role="form" action="<?php echo e(route ('category.destroy', $category->id)); ?>" method="POST">
				            		<?php echo method_field('DELETE'); ?>

					              <?php echo csrf_field(); ?>

										  	<button onclick="return areYouSure(event,this.form)" class="btn btn-pill-right btn-danger" type="submit"><em class="fa fa-eraser"></em></button>
				            	</form>
				            </div>
									</th>
								</tr>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</tbody>
						</table>
					</section>
				</div>
			</div>
		</div>
	</div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>