<?php $__env->startSection('content'); ?>
<div class="title-block">
  <h1 class="title"> Edit TAB</h1>
  <p class="title-description">Title : "<?php echo e($tab['title']); ?>"</p>
</div>
<?php if(session('message')): ?>
    <br/>
  <div class="alert alert-success">
      <?php echo e(session('message')); ?>

  </div>
  <br/>
<?php endif; ?>
<?php if($errors->any()): ?>
  <div class="alert alert-danger">
    <ul>
      <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <li><?php echo e($error); ?></li>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
  </div>
<?php endif; ?>
<section class="section">
  <div class="row sameheight-container">
    <div class="container-fluid">
        <div class="card card-block">
          <form action="<?php echo e(route('tab.update', [$event->id, $tab['tab_id']])); ?>" method="POST" enctype="multipart/form-data" role="form">
            <?php echo method_field('PUT'); ?>

            <?php echo csrf_field(); ?>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Title</label>
                  <input type="text" name="title" placeholder="" class="form-control underlined" value="<?php echo e(old('title',$tab['title'])); ?>"> 
                </div>
                <div class="form-group">
                  <label class="control-label">Columns</label>
                  <select name="container" class="selector_noxus">
                    <option value="<?php echo e(old('container',$tab['container'])); ?>" selected><?php echo e($tab['container']); ?></option>
                    <option value="full-width">Full Width</option>
                    <option value="small">Small</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Slug</label>
                  <input type="text" name="slug" placeholder="" class="form-control underlined" value="<?php echo e(old('title',$tab['slug'])); ?>"> 
                </div>
                <div class="form-group">
                  <label class="control-label">Status</label>
                  <select name="status" class="selector_noxus">
                    <option value="<?php echo e(old('status',$tab['status'])); ?>" selected><?php echo e($tab['status'] ? "Published" : "Unpublished"); ?></option>
                    <option value="1">Published</option>
                    <option value="0">Unpublished</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label">Content</label>
                  <div name="content" class="editor_noxus"><?php echo old('content',$tab['body_content']); ?></div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group pull-right">
                  <button type="submit" class="btn btn-info pull-right"><em class="fa fa-check-square-o"></em> Submit</button>
                  <a href="<?php echo e(route('event.edit', $event->id)); ?>" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-hand-o-left"></em> Cancel</a>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>