<?php echo $__env->make('admin.master.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<div class="app" id="app">
		<?php echo $__env->make('admin.master.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php echo $__env->make('admin.master.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<article class="content dashboard-page">
			<?php echo $__env->yieldContent('content'); ?>
		</article>
		<?php echo $__env->make('admin.master.menu_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	</div>
<?php echo $__env->make('admin.master.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
