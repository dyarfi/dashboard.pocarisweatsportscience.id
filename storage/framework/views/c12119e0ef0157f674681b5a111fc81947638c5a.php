<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-4">
		<div class="card card-primary">
			<div class="card-header">
				<div class="header-block">
					<p class="title" style="color:white;">Entry Big Article </p>
				</div>
			</div>
			<div class="card-block">
				<br/>
					<h1 class="text-center"><?php echo e($big_articles->count()); ?> <i class="fa fa-newspaper-o"></i></h1>
				<br/>			
			</div>
			<div class="card-footer">
				<a href="<?php echo e(url('big_article')); ?>" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-arrow-right"></em> Index</a>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card card-primary">
			<div class="card-header">
				<div class="header-block">
					<p class="title" style="color:white;"> Entry Article </p>
				</div>
			</div>
			<div class="card-block">
				<br/>
				<h1 class="text-center"><?php echo e($articles->count()); ?> <i class="fa fa-newspaper-o"></i></h1>
				<br/>
			</div>
			<div class="card-footer"> <a href="<?php echo e(url('article')); ?>" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-arrow-right"></em> Index</a></div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card card-primary">
			<div class="card-header">
				<div class="header-block">
					<p class="title" style="color:white;"> Entry Video </p>
				</div>
			</div>
			<div class="card-block">
				<br/>
					<h1 class="text-center"><?php echo e($videos->count()); ?> <i class="fa fa-video-camera"></i></h1>
				<br/>	
			</div>
			<div class="card-footer"> <a href="<?php echo e(url('video')); ?>" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-arrow-right"></em> Index</a></div>
		</div>
	</div>
</div>
<br/>
<div class="row">
	<div class="col-md-4">
		<div class="card card-primary">
			<div class="card-header">
				<div class="header-block">
					<p class="title" style="color:white;">Entry Slider </p>
				</div>
			</div>
			<div class="card-block">
				<br/>
					<h1 class="text-center"><?php echo e($sliders->count()); ?> <i class="fa fa-picture-o"></i></h1>
				<br/>			
			</div>
			<div class="card-footer">
				<a href="<?php echo e(url('slider')); ?>" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-arrow-right"></em> Index</a>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card card-primary">
			<div class="card-header">
				<div class="header-block">
					<p class="title" style="color:white;"> Entry Category </p>
				</div>
			</div>
			<div class="card-block">
				<br/>
				<h1 class="text-center"><?php echo e($categories->count()); ?> <i class="fa fa-pencil-square-o"></i></h1>
				<br/>
			</div>
			<div class="card-footer"> <a href="<?php echo e(url('category')); ?>" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-arrow-right"></em> Index</a></div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card card-primary">
			<div class="card-header">
				<div class="header-block">
					<p class="title" style="color:white;"> Entry Event </p>
				</div>
			</div>
			<div class="card-block">
				<br/>
					<h1 class="text-center"><?php echo e($events->count()); ?> <i class="fa fa-calendar" aria-hidden="true"></i></h1>
				<br/>	
			</div>
			<div class="card-footer"> <a href="<?php echo e(url('event')); ?>" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-arrow-right"></em> Index</a></div>
		</div>
	</div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>