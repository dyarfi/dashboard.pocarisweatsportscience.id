<?php $__env->startSection('content'); ?>
<div class="title-block">
	<h1 class="title"> Article </h1>
	<p class="title-description"> Index Table Article </p>
	<a href="<?php echo e(route('article.create')); ?>" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-plus"></em> Add</a>
</div>

<?php if(session('message')): ?>
  <div class="alert alert-success">
	  <?php echo e(session('message')); ?>

  </div>
<?php endif; ?>
<section class="section">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-block">
					<div class="card-title-block">
					</div>
					<section class="example">
						<table class="table table_noxus table-hover">
							<thead>
								<tr>
									<th>Image</th>
									<th>Title</th>
									<th>Type</th>
									<th>Category</th>
									<th>Date</th>
									<th>Tag</th>
									<th>Status</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php $__currentLoopData = $articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<tr>
									<th><img class="thumb_noxs" src="<?php echo e(asset('uploads')); ?>/<?php echo e($article->image); ?>"/></th>
									<th><?php echo e($article->title); ?></th>
									<th><?php echo e($article->type); ?></th>
									<th><?php echo e($article->category); ?></th>
									<th>
										<?php if($article->date): ?>
										<?php 
											$utcdatetime = $article->date;
										  $datetime = $utcdatetime->toDateTime();
										  $time=$datetime->format('m/d/Y H:m:s');
										?>
										<?php echo e($time); ?>

										<?php endif; ?>
									</th>
									<th><?php echo e($article->tag); ?></th>
									<th><?php echo e($article->status ? "Published" : "Unpublished"); ?></th>
									<th width="150px;">
										<div class="actions_noxus">
											<a class="button_left" href="<?php echo e(url('article')); ?>/<?php echo e($article->id); ?>/edit">
												<button type="button" class="btn btn-pill-left btn-info"><em class="fa fa-pencil-square-o"></em></button>
											</a>
					          	<form class="button_right" role="form" action="<?php echo e(route ('article.destroy', $article->id)); ?>" method="POST">
				            		<?php echo method_field('DELETE'); ?>

					              <?php echo csrf_field(); ?>

										  	<button onclick="return areYouSure(event,this.form)" class="btn btn-pill-right btn-danger" type="submit"><em class="fa fa-eraser"></em></button>
				            	</form>
				            </div>
									</th>
								</tr>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</tbody>
						</table>
					</section>
				</div>
			</div>
		</div>
	</div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>