<?php echo $__env->make('admin.master.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="auth">
  <div class="auth-container">
    <div class="card">
      <header class="auth-header">
        <h1 class="auth-title">
          <div class="logo">
            <img src="<?php echo e(asset('logo.png')); ?>"/>
          </div>
        </h1>
        </header>
        <div class="auth-content">
          <p class="text-center">DASHBOARD</p>
            <form class="form-horizontal" method="POST" action="<?php echo e(route('login')); ?>">
            <?php echo e(csrf_field()); ?>

            <div class="form-group">
              <label for="username">Username</label>
              <input type="text" class="form-control underlined" name="username" id="username" placeholder="Input your username" required> 
              <?php if($errors->has('email')): ?>
                <span class="help-block">
                  <strong><?php echo e($errors->first('email')); ?></strong>
                </span>
              <?php endif; ?>
            </div>
              <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control underlined" name="password" id="password" placeholder="Your password" required>
                <?php if($errors->has('password')): ?>
                <span class="help-block">
                  <strong><?php echo e($errors->first('password')); ?></strong>
                </span>
                <?php endif; ?>
              </div>
              <br/>
              <div class="form-group">
                <button type="submit" class="btn btn-block btn-primary">Login</button>
              </div>
              </form>
            </div>
          </div>
        </div>
        </div>
<?php echo $__env->make('admin.master.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>