<?php $__env->startSection('content'); ?>
<div class="title-block">
	<h1 class="title"> Slider </h1>
	<p class="title-description"> Index Table Slider </p>
	<a href="<?php echo e(route('slider.create')); ?>" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-plus"></em> Add</a>
</div>

<?php if(session('message')): ?>
  <div class="alert alert-success">
	  <?php echo e(session('message')); ?>

  </div>
<?php endif; ?>
<section class="section">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-block">
					<div class="card-title-block">
					</div>
					<section class="example">
						<table class="table table_noxus table-hover">
							<thead>
								<tr>
									<th>Image</th>
									<th>Image Mobile</th>
									<th>Url</th>
									<th>Order Number</th>
									<th>Status</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php $__currentLoopData = $sliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slider): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<tr>
									<th><img class="thumb_noxs" src="<?php echo e(asset('uploads')); ?>/<?php echo e($slider->image); ?>"/></th>
									<th><img class="thumb_noxs" src="<?php echo e(asset('uploads')); ?>/<?php echo e($slider->image_mobile); ?>"/></th>
									<th style="max-width:200px;"><?php echo e($slider->url); ?></th>
									<th><?php echo e($slider->order_number); ?></th>
									<th><?php echo e($slider->status ? "True" : "False"); ?></th>
									<th style="width:150px;">
										<div class="actions_noxus">
											<a class="button_left" href="<?php echo e(url('slider')); ?>/<?php echo e($slider->id); ?>/edit">
												<button type="button" class="btn btn-pill-left btn-info"><em class="fa fa-pencil-square-o"></em></button>
											</a>
					          	<form class="button_right" role="form" action="<?php echo e(route ('slider.destroy', $slider->id)); ?>" method="POST">
				            		<?php echo method_field('DELETE'); ?>

					              <?php echo csrf_field(); ?>

										  	<button onclick="return areYouSure(event,this.form)" class="btn btn-pill-right btn-danger" type="submit"><em class="fa fa-eraser"></em></button>
				            	</form>
				            </div>
									</th>
								</tr>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</tbody>
						</table>
					</section>
				</div>
			</div>
		</div>
	</div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>