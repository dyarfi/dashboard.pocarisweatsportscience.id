<?php $__env->startSection('content'); ?>
<div class="title-block">
	<h1 class="title"> Admin User </h1>
	<p class="title-description"> Index Table Admin Dashboard </p>
		<a href="<?php echo e(route('admin_user.create')); ?>" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-plus"></em> Add</a>
</div>
<?php if(session('message')): ?>
	<br/>
  <div class="alert alert-success">
	  <?php echo e(session('message')); ?>

  </div>
  <br/>
<?php endif; ?>
<section class="section">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-block">
					<div class="card-title-block">
					</div>
					<section class="example">
						<table class="table table_noxus_ table-hover">
							<thead>
								<tr>
									<th>Name</th>
									<th>Username</th>
									<th>Email</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<tr>
									<th><?php echo e($user->name); ?></th>
									<th><?php echo e($user->username); ?></th>
									<th><?php echo e($user->email); ?></th>
									<th>
										<div class="actions_noxus">
											<a class="button_left" href="<?php echo e(url('admin_user')); ?>/<?php echo e($user->id); ?>/edit">
												<button type="button" class="btn btn-pill-left btn-info"><em class="fa fa-pencil-square-o"></em></button>
											</a>
					          	<form role="form" class="button_right" action="<?php echo e(route ('admin_user.destroy', $user->id)); ?>" method="POST">
				            		<?php echo method_field('DELETE'); ?>

					              <?php echo csrf_field(); ?>

										  	<button type="button" onclick="return areYouSure(event,this.form)" class="btn btn-pill-right btn-danger"><em class="fa fa-eraser"></em></button>
				            	</form>
										</div>
									</th>
								</tr>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</tbody>
						</table>
					</section>
				</div>
			</div>
		</div>
	</div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>