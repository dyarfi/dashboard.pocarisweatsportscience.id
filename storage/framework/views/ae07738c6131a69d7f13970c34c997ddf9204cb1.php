    <form action="<?php echo e(route('event.update', $event->id)); ?>" method="POST" enctype="multipart/form-data" role="form">
      <?php echo method_field('PUT'); ?>

      <?php echo csrf_field(); ?>

    <section class="section">
      <div class="row sameheight-container">
        <div class="container-fluid">
          <div class="card card-block">
            <div class="row">
              <div class="col-md-12">
                <div class="title-block">
                  <h1 class="title"> <b>Event Detail</b></h1>
                  <p class="title-description"><hr/></p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Title</label>
                  <input type="text" name="title" placeholder="" class="form-control underlined" value="<?php echo e(old('title',$event->title)); ?>"> 
                </div>
                <div class="form-group">
                  <label class="control-label">Columns</label>
                  <select name="columns" class="selector_noxus">
                    <option value="<?php echo e(old('columns',$event->columns)); ?>" selected><?php echo e($event->columns); ?></option>
                    <option value="big">Big</option>
                    <option value="small">Small</option>
                  </select>
                </div>
                <div class="form-group">
                  <label class="control-label">Status</label>
                  <select name="status" class="selector_noxus">
                    <option value="<?php echo e(old('status',$event->status)); ?>" selected><?php echo e($event->status ? "Published" : "Unpublished"); ?></option>
                    <option value="1">Published</option>
                    <option value="0">Unpublished</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Tag</label>
                  <input type="text" name="tag" placeholder="" class="form-control underlined" value="<?php echo e(old('tag',$event->tag)); ?>"> 
                </div>
                <div class="form-group">
                  <label class="control-label">Header Text</label>
                  <select name="header_type" class="selector_noxus">
                    <option value="<?php echo e(old('header_type',$event->header_type)); ?>" selected><?php echo e($event->header_type); ?></option>
                    <option value="text-right">text-right</option>
                    <option value="text-left">text-left</option>
                    <option value="text-center">text-center</option>
                    <option value="without-text">without-text</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br/>
      <div class="row sameheight-container">
        <div class="container-fluid">
          <div class="card card-block">
            <div class="row">
              <div class="col-md-12">
                <div class="title-block">
                  <h1 class="title"> <b>Images</b></h1>
                  <p class="title-description"><hr/></p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="thumb_image cont">
                  <img src="<?php echo e(asset('uploads')); ?>/<?php echo e($event->image); ?>" alt="" class="thumb_image"/>
                  <span><?php echo e($event->image); ?></span>
                </div>
                <div class="form-group">
                  <label class="control-label">Image(1200x530)</label>
                  <input type="file" name="image" placeholder="" class="form-control underlined" value="">
                </div>
                <div class="thumb_image cont">
                  <img src="<?php echo e(asset('uploads')); ?>/<?php echo e($event->image_thumbnail); ?>" alt="" class="thumb_image"/>
                  <span><?php echo e($event->image_thumbnail); ?></span>
                </div>
                <div class="form-group">
                  <label class="control-label">Image Thumbnail(Potrait)</label>
                  <input type="file" name="image_thumbnail" placeholder="" class="form-control underlined" value="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="thumb_image cont">
                  <img style="width:100%;" src="<?php echo e(asset('uploads')); ?>/<?php echo e($event->image_background); ?>" alt="" class="thumb_image"/>
                  <span><?php echo e($event->image_background); ?></span>
                </div>
                <div class="form-group">
                  <label class="control-label">Image Background(1240x960)</label>
                  <input type="file" name="image_background" placeholder="" class="form-control underlined" value="">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br/>
      <div class="row sameheight-container">
        <div class="container-fluid">
          <div class="card card-block">
            <div class="row">
              <div class="col-md-12">
                <div class="title-block">
                  <h1 class="title"> <b>Event Information</b></h1>
                  <p class="title-description"><hr/></p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label">Content</label>
                  <div name="content[body_content]" class="editor_noxus">
                    <?php echo old('content[body_content]',$information['body_content']); ?>

                  </div>
                </div>
              </div>
            </div>
            <hr/>
          </div>
        </div>
      </div>
      <br/>
      <div class="row sameheight-container">
        <div class="container-fluid">
          <div class="card card-block">
            <div class="row">
              <div class="col-md-12">
                <div class="title-block">
                  <h1 class="title"> <b>CTA</b></h1>
                  <p class="title-description"><hr/></p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label">Button Title</label>
                  <input type="text" name="content[body_button][0][btn_title]" placeholder='<i class="fa fa-phone"></i> Abay <br> 0813 8036 7630' class="form-control underlined" value="<?php echo e(old('content[body_button][0][btn_title]', $information['body_button'][0]['btn_title'])); ?>"> 
                </div>
                <div class="form-group">
                  <label class="control-label">Button Link</label>
                  <input type="text" name="content[body_button][0][btn_url]" placeholder="tel:0211234567" class="form-control underlined" value="<?php echo e(old('content[body_button][0][btn_url]', $information['body_button'][0]['btn_url'])); ?>"> 
                </div>
                <div class="form-group">
                  <label class="control-label">Columns</label>
                  <input type="text" name="content[body_button][0][columns]" placeholder="col-md-4" class="form-control underlined" value="<?php echo e(old('content[body_button][0][columns]', $information['body_button'][0]['columns'])); ?>"> 
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label">Button Title</label>
                  <input type="text" name="content[body_button][1][btn_title]" placeholder='<i class="fa fa-phone"></i> Abay <br> 0813 8036 7630' class="form-control underlined" value="<?php echo e(old('content[body_button][1][btn_title]', $information['body_button'][1]['btn_title'])); ?>"> 
                </div>
                <div class="form-group">
                  <label class="control-label">Button Link</label>
                  <input type="text" name="content[body_button][1][btn_url]" placeholder="tel:0211234567" class="form-control underlined" value="<?php echo e(old('content[body_button][1][btn_url]', $information['body_button'][1]['btn_url'])); ?>"> 
                </div>
                <div class="form-group">
                  <label class="control-label">Columns</label>
                  <input type="text" name="content[body_button][1][columns]" placeholder="col-md-4" class="form-control underlined" value="<?php echo e(old('content[body_button][1][columns]', $information['body_button'][1]['columns'])); ?>"> 
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label">Button Title</label>
                  <input type="text" name="content[body_button][2][btn_title]" placeholder='<i class="fa fa-phone"></i> Abay <br> 0813 8036 7630' class="form-control underlined" value="<?php echo e(old('content[body_button][2][btn_title]', $information['body_button'][2]['btn_title'])); ?>"> 
                </div>
                <div class="form-group">
                  <label class="control-label">Button Link</label>
                  <input type="text" name="content[body_button][2][btn_url]" placeholder="tel:0211234567" class="form-control underlined" value="<?php echo e(old('content[body_button][2][btn_url]', $information['body_button'][2]['btn_url'])); ?>"> 
                </div>
                <div class="form-group">
                  <label class="control-label">Columns</label>
                  <input type="text" name="content[body_button][2][columns]" placeholder="col-md-4" class="form-control underlined" value="<?php echo e(old('content[body_button][2][columns]', $information['body_button'][2]['columns'])); ?>"> 
                </div>
              </div>
            </div>
            <hr/>
          </div>
        </div>
      </div>
      <br/>
      <div class="row sameheight-container">
        <div class="container-fluid">
          <div class="card card-block">
            <div class="row">
              <div class="col-md-12">
                <div class="title-block">
                  <h1 class="title"><b>Related Tabs</b></h1>
                  <p class="title-description"><hr/></p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Content Tag Article</label>
                  <input type="text" name="content_tag_article" placeholder="Insert Tag Articles" class="form-control underlined" value="<?php echo e(old('content[1][content_tag]', $article['content_tag'])); ?>"> 
                </div> 
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Content Tag Gallery</label>
                  <input type="text" name="content_tag_gallery" placeholder="Insert Tag Gallery" class="form-control underlined" value="<?php echo e(old('content[2][content_tag]', $gallery['content_tag'])); ?>"> 
                </div> 
              </div>
            </div>        
          </div>
        </div>
      </div>
      <br/>
      <div class="row sameheight-container">
        <div class="container-fluid">
          <div class="card card-block">
            <div class="row">
              <div class="col-md-12">
                <div class="title-block">
                  <h1 class="title"><b>Left Sidebar</b></h1>
                  <p class="title-description"><hr/></p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label">Event Type</label>
                  <input type="hidden" name="content[left_sidebar][list][0][field]" placeholder="" class="form-control underlined" value="Event Type"> 
                  <input type="text" name="content[left_sidebar][list][0][value]" placeholder="Futsal" class="form-control underlined" value="<?php echo e(old('content[left_sidebar][0][list][0][value]', $information['left_sidebar']['0']['list']['0']['value'])); ?>"> 
                </div> 
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label">Venue</label>
                  <input type="hidden" name="content[left_sidebar][list][1][field]" placeholder="" class="form-control underlined" value="Venue"> 
                  <input type="text" name="content[left_sidebar][list][1][value]" placeholder="GOR C-TRA ARENA" class="form-control underlined" value="<?php echo e(old('content[left_sidebar][0][list][1][value]', $information['left_sidebar']['0']['list']['1']['value'])); ?>"> 
                </div> 
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label">Date</label>
                  <input type="hidden" name="content[left_sidebar][list][2][field]" placeholder="" class="form-control underlined" value="Date"> 
                  <input type="text" name="content[left_sidebar][list][2][value]" placeholder="example : 12-15 January 2020" class="form-control underlined" value="<?php echo e(old('content[left_sidebar][0][list][2][value]', $information['left_sidebar']['0']['list']['2']['value'])); ?>"> 
                </div> 
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group pull-right">
                  <button type="submit" class="btn btn-info pull-right"><em class="fa fa-check-square-o"></em> Submit</button>
                  <a href="<?php echo e(route('event.index')); ?>" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-hand-o-left"></em> Cancel</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      </section>
      <input type="hidden" name="content[left_sidebar][title]" placeholder="" class="form-control underlined" value="EVENT INFORMATION"> 
      <input type="hidden" name="content[left_sidebar][type]" placeholder="" class="form-control underlined" value="vertical">
      <input type="hidden" name="content[status]" placeholder="" class="form-control underlined" value="1"> 
      <input type="hidden" name="content[type]" placeholder="" class="form-control underlined" value="a"> 
    </form>