<?php $__env->startSection('content'); ?>
<div class="title-block">
    <h1 class="title"> Edit Category</h1>
    <p class="title-description">  Name : <?php echo e($category->name); ?> </p>
</div>
<?php if(session('message')): ?>
    <br/>
  <div class="alert alert-success">
      <?php echo e(session('message')); ?>

  </div>
  <br/>
<?php endif; ?>
<section class="section">
  <div class="row sameheight-container">
    <div class="container-fluid">
        <div class="card card-block sameheight-item">
          <form action="<?php echo e(route ('category.update', $category->id)); ?>" method="POST" role="form">
            <?php echo method_field('PUT'); ?>

            <?php echo csrf_field(); ?>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Name</label>
                  <input type="text" name="name" placeholder="<?php echo e(old ('name', $category->name)); ?>" class="form-control underlined" value="<?php echo e(old ('name', $category->name)); ?>"> 
                </div>
                <div class="form-group">
                  <label class="control-label">Alias</label>
                  <input type="text" name="alias" placeholder="<?php echo e(old ('alias', $category->alias)); ?>" class="form-control underlined" value="<?php echo e(old ('alias', $category->alias)); ?>"> 
                </div>
                <div class="form-group">
                  <label class="control-label">Type</label>
                  <input type="text" name="type" placeholder="<?php echo e(old ('type', $category->type)); ?>" class="form-control underlined" value="<?php echo e(old ('type', $category->type)); ?>"> 
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Order Number</label>
                  <input type="text" name="order_number" placeholder="<?php echo e(old ('order_number', $category->order_number)); ?>" class="form-control underlined" value="<?php echo e(old ('order_number', $category->order_number)); ?>"> 
                </div>
                <div class="form-group">
                  <label class="control-label">Status</label>
                  <select name="status" class="selector_noxus">
                    <option value="<?php echo e(old ('status', $category->status)); ?>" selected><?php echo e(old ('status', $category->status) == '1' ? 'True' : 'False'); ?></option>
                    <option value="1">True</option>
                    <option value="0">False</option>
                  </select>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-info pull-right"><em class="fa fa-check-square-o"></em> Submit</button>
            <a href="<?php echo e(route('category.index')); ?>" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-hand-o-left"></em> Cancel</a>
          </form>
        </div>
      </div>
    </div>
  </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>