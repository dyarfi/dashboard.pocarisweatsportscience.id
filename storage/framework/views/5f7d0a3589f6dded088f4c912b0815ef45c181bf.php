<?php $__env->startSection('content'); ?>
<div class="title-block">
  <h1 class="title"> Create Article</h1>
  <p class="title-description"></p>
</div>
<?php if(session('message')): ?>
    <br/>
  <div class="alert alert-success">
      <?php echo e(session('message')); ?>

  </div>
  <br/>
<?php endif; ?>
<?php if($errors->any()): ?>
  <div class="alert alert-danger">
    <ul>
      <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <li><?php echo e($error); ?></li>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
  </div>
<?php endif; ?>
<section class="section">
  <div class="row sameheight-container">
    <div class="container-fluid">
        <div class="card card-block">
          <form action="<?php echo e(action ('Admin\Article\ArticleController@store')); ?>" enctype="multipart/form-data" method="POST" role="form">
            <?php echo csrf_field(); ?>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Title</label>
                  <input type="text" name="title" placeholder="" class="form-control underlined" value=""> 
                </div>
                <div class="form-group">
                  <label class="control-label">Category</label>
                  <select name="category" class="selector_noxus">
                    <option value="" selected>Select Your Category</option>
                    <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($category->name); ?>"><?php echo e($category->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </select>
                </div>
                <div class="form-group">
                  <label class="control-label">Date</label>
                  <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                    <input  name="date" value="" class="form-control datetimepicker-input underlined" data-target="#datetimepicker1"  />
                    <span class="input-group-addon" data-target="#datetimepicker1" data-toggle="datetimepicker">
                    <span class="fa fa-calendar"></span>
                    </span>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Caption</label>
                  <input type="text" name="caption" placeholder="" class="form-control underlined" value=""> 
                </div>
                <div class="form-group">
                  <label class="control-label">Tag</label>
                  <input type="text" name="tag" placeholder="" class="form-control underlined" value=""> 
                </div>
                <div class="form-group">
                  <label class="control-label">Writer</label>
                  <input type="text" name="writer[name]" placeholder="" class="form-control underlined" value=""> 
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label">Content</label>
                  <div name="content" class="editor_noxus"></div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Columns</label>
                  <select name="columns" class="selector_noxus">
                    <option value="" selected>Select Your Column Size</option>
                    <option value="big">Big</option>
                    <option value="small">Small</option>
                  </select>
                </div>
                <div class="form-group">
                  <label class="control-label">Image(1200x530)</label>
                  <input type="file" name="image" placeholder="" class="form-control underlined" value="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">Header Text</label>
                  <select name="header_type" class="selector_noxus">
                    <option value="" selected>Select Your Header Text</option>
                    <option value="text-right">text-right</option>
                    <option value="text-left">text-left</option>
                    <option value="text-center">text-center</option>
                    <option value="without-text">without-text</option>
                  </select>
                </div>
                <div class="form-group">
                  <label class="control-label">Status</label>
                  <select name="status" class="selector_noxus">
                    <option value="" selected>Select Your Status</option>
                    <option value="1">Published</option>
                    <option value="0">Unpublished</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group pull-right">
                  <button type="submit" class="btn btn-info pull-right"><em class="fa fa-check-square-o"></em> Submit</button>
                  <a href="<?php echo e(route('article.index')); ?>" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-hand-o-left"></em> Cancel</a>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>