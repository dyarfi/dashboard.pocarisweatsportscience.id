      <aside class="sidebar">
        <div class="sidebar-container">
          <div class="sidebar-header">
            <div class="brand">
              <div class="logo">
                <img src="<?php echo e(asset('logo.png')); ?>"/>
              </div>
            </div>
          </div>
          <nav class="menu">
            <ul class="sidebar-menu metismenu" id="sidebar-menu">
              <li <?php echo e(setActive('/')); ?>>
                <a href="<?php echo e(url('/')); ?>">
                <i class="fa fa-home"></i> Dashboard </a>
              </li>
              <li <?php echo e(setActive('admin_user')); ?>>
                <a href="<?php echo e(url('admin_user')); ?>">
                <i class="fa fa-user"></i> Admin </a>
              </li>
              <li <?php echo e(setActive('category')); ?>>
                <a href="<?php echo e(url('category')); ?>">
                <i class="fa fa-pencil-square-o"></i> Category </a>
              </li>
              <li <?php echo e(setActive('slider')); ?>>
                <a href="<?php echo e(url('slider')); ?>">
                <i class="fa fa-picture-o"></i> Slider </a>
              </li>
              <li <?php echo e(setActive('article')); ?>>
                <a href="<?php echo e(url('article')); ?>">
                <i class="fa fa-newspaper-o"></i> Article </a>
              </li>
              <li <?php echo e(setActive('big_article')); ?>>
                <a href="<?php echo e(url('big_article')); ?>">
                <i class="fa fa-newspaper-o"></i> Big Article </a>
              </li>
              <li <?php echo e(setActive('event')); ?>>
                <a href="<?php echo e(url('event')); ?>">
                <i class="fa fa-calendar" aria-hidden="true"></i> Event </a>
              </li>
              <li <?php echo e(setActive('video')); ?>>
                <a href="<?php echo e(url('video')); ?>">
                <i class="fa fa-video-camera"></i> Video </a>
              </li>
<!--               <li class="open">
                <a href="">
                    <i class="fa fa-sitemap"></i> Menu Levels
                    <i class="fa arrow"></i>
                </a>
                <ul class="sidebar-nav collapse in" style="">
                  <li>
                    <a href="#"> Second Level Item </a>
                  </li>
                </ul>
              </li> -->
            </ul>
          </nav>
        </div>
      </aside>
      <div class="sidebar-overlay" id="sidebar-overlay"></div>
      <div class="sidebar-mobile-menu-handle" id="sidebar-mobile-menu-handle"></div>
      <div class="mobile-menu-handle"></div>