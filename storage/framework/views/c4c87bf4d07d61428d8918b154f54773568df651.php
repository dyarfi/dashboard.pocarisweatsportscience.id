<?php $__env->startSection('content'); ?>
<div class="title-block">
	<h1 class="title"> Big Article | Campaign </h1>
	<p class="title-description"> Index Table Big Article or Campaign </p>
	<a href="<?php echo e(route('big_article.create')); ?>" class="btn btn-info pull-right" style="margin-right:5px;"><em class="fa fa-plus"></em> Add</a>
</div>

<?php if(session('message')): ?>
  <div class="alert alert-success">
	  <?php echo e(session('message')); ?>

  </div>
<?php endif; ?>
<section class="section">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-block">
					<div class="card-title-block">
					</div>
					<section class="example">
						<table class="table table_noxus table-hover">
							<thead>
								<tr>
									<th>Title</th>
									<th>Image</th>
									<th>Campaign Name</th>
									<th>Slug</th>
									<th>Date</th>
									<th>Type</th>
									<th>Status</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php $__currentLoopData = $big_articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<tr>
									<th><?php echo e($article->title); ?></th>
									<th><img class="thumb_noxs" src="<?php echo e(asset('uploads')); ?>/<?php echo e($article->campaign['image']); ?>"/></th>
									<th><?php echo e($article->campaign['name']); ?></th>
									<th><?php echo e($article->slug); ?></th>
									<th>
										<?php if($article->date): ?>
										<?php 
											$utcdatetime = $article->date;
										  $datetime = $utcdatetime->toDateTime();
										  $time=$datetime->format('m/d/Y H:m:s');
										?>
										<?php echo e($time); ?>

										<?php endif; ?>
									</th>
									<th><?php echo e($article->type); ?></th>
									<th><?php echo e($article->status ? "Published" : "Unpublished"); ?></th>
									<th width="150px;">
										<div class="actions_noxus">
											<a class="button_left" href="<?php echo e(url('big_article')); ?>/<?php echo e($article->id); ?>/edit">
												<button type="button" class="btn btn-pill-left btn-info"><em class="fa fa-pencil-square-o"></em></button>
											</a>
					          	<form class="button_right" role="form" action="<?php echo e(route ('big_article.destroy', $article->id)); ?>" method="POST">
				            		<?php echo method_field('DELETE'); ?>

					              <?php echo csrf_field(); ?>

										  	<button onclick="return areYouSure(event,this.form)" class="btn btn-pill-right btn-danger" type="submit"><em class="fa fa-eraser"></em></button>
				            	</form>
				            </div>
									</th>
								</tr>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</tbody>
						</table>
					</section>
				</div>
			</div>
		</div>
	</div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>