<?php $__env->startSection('content'); ?>
<div class="title-block">
  <h1 class="title"> Edit Event</h1>
  <p class="title-description">Titled : <?php echo e($event->title); ?></p>
  <span>Total Tab : <?php echo e($count_tabs); ?></span>
</div>
<?php if(session('message')): ?>
    <br/>
  <div class="alert alert-success">
      <?php echo e(session('message')); ?>

  </div>
  <br/>
<?php endif; ?>
<?php if($errors->any()): ?>
  <div class="alert alert-danger">
    <ul>
      <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <li><?php echo e($error); ?></li>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
  </div>
<?php endif; ?>
<section class="section">
    <ul class="nav nav-tabs nav-tabs-bordered">
      <li class="nav-item">
        <a href="#home" class="nav-link active" data-target="#home" data-toggle="tab" aria-controls="home" role="tab" aria-expanded="true">Event</a>
      </li>
      <li class="nav-item">
        <a href="#tab-index" class="nav-link" data-target="#tab-index" aria-controls="tab-index" data-toggle="tab" role="tab" aria-expanded="false">Tab Index</a>
      </li>
    </ul>
</section>
<!-- Tab panes -->
<div class="tab-content tabs-bordered">
  <div class="tab-pane fade in active show" id="home" aria-expanded="true">
    <br/>
    <?php echo $__env->make('admin.event.tabs.event_information', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </div>
    <?php echo $__env->make('admin.event.tabs.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>

  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>