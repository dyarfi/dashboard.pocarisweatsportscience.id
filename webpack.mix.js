let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
// Browsersync hot reload
mix.browserSync({
  proxy:'dashboard-pocarisweatsportscience.test',
  // Open the site in Chrome & Firefox
  // browser: ["google chrome", "firefox"],
  browser: ["firefox"],
  // browser: ["google chrome"],
  // Don't show any notifications in the browser.
  notify: false,
  files: [
      "public/admin/js/**/*.js",
      "public/admin/css/**/*.css",
      "app/Http/**/*.php",
      "resources/assets/**/*.*",
      "resources/views/**/*.php",
      "resources/views/*.php",
      "resources/lang/**/*.php",
  ]
});
mix.disableNotifications();

// JS Apps / React
//mix.js('public/admin/js/noxus.js', 'public/admin/js');

mix.styles([
	'public/admin/css/vendor.css',
	'public/admin/css/app-blue.css',
	'public/admin/css/jquery.dataTables.min.css',
	'public/admin/css/responsive.datatables.css',
	'public/admin/css/bootstrap.datetimepicker.css',
	'public/admin/css/sweetalert2.min.css',
	'public/admin/css/select2.min.css',
	'public/admin/css/noxus.css',
	], 'public/admin/css/noxus_compiled.css'),
mix.scripts([
	'public/admin/js/vendor.js',
	'public/admin/js/app.js',
	'public/admin/js/jquery.dataTables.min.js',
	'public/admin/js/moment.js',
	'public/admin/js/datatables.responsive.js',
	'public/admin/js/select2.min.js',
	'public/admin/js/tinymce.min.js',
	'public/admin/js/tempusdominus-bootstrap-4.js',
	'public/admin/js/sweetalert2.js',
	'public/admin/js/noxus.js',
	],'public/admin/js/noxus_compiled.js')
